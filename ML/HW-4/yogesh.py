def dot_prod_in_kernal_space(x, c, kernel):
    if kernel == 'rbf':
        return math.exp(-np.linalg.norm(x - c) ** 2)
    if kernel == 'quadratic':
        return (1 + np.dot(x, c)) ** 2
    if kernel == 'linear':
        return np.dot(x, c)


def get_gamma(assignments, k):
    gamma = np.zeros((len(assignments), k));
    for i in range(len(assignments)):
        gamma[i][assignments[i]] = 1

    sums = np.sum(gamma, axis=0)
    return gamma / sums


def find_cluster(data_point, clusters):
    distances = list(map(lambda cluster: get_distance(data_point, cluster), clusters))
    return np.argmin(distances)


def get_kernel_distances(data, gamma, i, cluster, kernel, dot_products):
    print(x[i])
    prod = dot_prod_in_kernal_space(x[i], x[i], kernel)
    q = 0
    n = len(x)
    for k in range(n):
        q += gamma[k][cluster] * dot_products[i][k]

    r = 0
    for k in range(n):
        for l in range(n):
            r += gamma[k][cluster] * gamma[l][cluster] * dot_products[k][l]

    return prod + r - 2 * q


def get_new_cluster_assignments(data, cluster_assignments, k, kernel):
    n = len(data)
    dot_products = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            dot_products[i][j] = dot_prod_in_kernal_space(data[i], data[j], kernel)

    alpha = get_gamma(cluster_assignments, k)
    distances = np.zeros((n, k))
    for i in range(n):
        for j in range(k):
            distances[i][j] = get_kernel_distances(data, alpha, i, j, kernel, dot_products)

    cluster_assignments_new = np.argmin(distances, axis=1)
    return cluster_assignments_new


def get_kernal_clusters(data, num_of_cluster, kernel='linear'):
    dim = len(data[0])
    clusters = np.random.standard_normal((num_of_cluster, dim))
    new_cluster_assignments = np.array(list(map(lambda point: find_cluster(point, clusters), data)))
    prev_cluster_assignments = np.zeros((num_of_cluster, dim))

    while (not np.array_equal(new_cluster_assignments, prev_cluster_assignments)):
        prev_cluster_assignments = new_cluster_assignments
        new_cluster_assignments = get_new_cluster_assignments(data, prev_cluster_assignments, num_of_cluster, kernel)

    return new_cluster_assignments
