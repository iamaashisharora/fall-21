import numpy as np
import scipy.io
import torch.nn
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from torch import nn, optim

NUMBER_HIDDEN_NODES = 2500
NUM_OF_FEATURES = 90
BATCH_SIZE = 1024


class DenseModel(nn.Module):
    def __init__(self):
        super(DenseModel, self).__init__()
        self.first_layer = torch.nn.Linear(NUM_OF_FEATURES, 100)
        # self.middle_layer = torch.nn.Linear(200, 100)
        self.linear2 = torch.nn.Linear(100, NUM_OF_FEATURES)
        self.relu = torch.nn.Tanh()
        self.dropout = torch.nn.Dropout(p=0.2)
        self.softmax = torch.nn.Softmax(1)

    def forward(self, x):
        linear1_output = self.first_layer(x)
        first_layer_output = self.relu(linear1_output)
        dropout_output = self.dropout(first_layer_output)
        second_layer_output = self.linear2(dropout_output)
        softmax_output = self.softmax(second_layer_output)
        return softmax_output


def train_predict_report(model, loss_fn, optimizer, train_x, train_y, dev_x, dev_y):
    for epoch in range(10):
        total_dev_loss = 0
        for i in range(0, len(train_x), BATCH_SIZE):
            model.zero_grad()
            train_y_pred = model(train_x[i:i + BATCH_SIZE])
            train_loss = loss_fn(train_y_pred, train_y[i:i + BATCH_SIZE])

            train_loss.backward()
            optimizer.step()

        with torch.no_grad():
            for i in range(0, len(dev_x), BATCH_SIZE):
                test_y_pred = model(dev_x[i:i + BATCH_SIZE])
                dev_loss = loss_fn(test_y_pred, dev_y[i:i + BATCH_SIZE])
                total_dev_loss += dev_loss.item()

        print("Validation Loss for Epoch " + str(epoch) + ": " + str(total_dev_loss))

    return model


def test_model(model, loss_fn, test_x, test_y):
    gold = []
    predicted = []

    loss = torch.zeros(1)
    model.eval()

    with torch.no_grad():
        for i in range(0, len(test_x), BATCH_SIZE):
            y_pred = model(test_x[i:i + BATCH_SIZE])

            gold.extend(test_y[i:i + BATCH_SIZE].cpu().detach().numpy())
            predicted.extend(y_pred.argmax(1).cpu().detach().numpy())

            loss += loss_fn(y_pred.double(), test_y[i:i + BATCH_SIZE].long()).data

    print("Mean Absolute Error: ")
    print(mean_absolute_error(gold, predicted))


def normalize_data(data):
    scaler = MinMaxScaler()
    return scaler.fit_transform(data)


def get_label(data):
    label = np.empty(len(data))
    min = data.min()
    for i, d in enumerate(data):
        label[i] = d[0] - min
    return label


def read_data():
    data = scipy.io.loadmat('./data/MSdata.mat')
    training_data = normalize_data(data['trainx'])
    testing_data = normalize_data(data['testx'])
    training_label = get_label(data['trainy'])
    return training_data, training_label, testing_data


if __name__ == '__main__':
    X, Y, test_X = read_data()
    train_X, dev_x, train_Y, dev_y = train_test_split(X, Y, test_size=0.2, random_state=13)
    train_x, test_x, train_y, test_y = train_test_split(train_X, train_Y, test_size=0.2, random_state=13)
    model = DenseModel().double()
    optimizer = optim.Adam(model.parameters(), lr=0.03)
    loss = nn.CrossEntropyLoss()
    train_predict_report(model, loss, optimizer, torch.from_numpy(train_x).double(),
                         torch.tensor(train_y).long(), torch.from_numpy(dev_x).double(), torch.tensor(dev_y).long())
    test_model(model, loss, torch.from_numpy(test_x).double(), torch.tensor(test_y).long())
