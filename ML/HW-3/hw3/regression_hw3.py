import os
import pickle

import numpy
import scipy.io
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

TRAINING_DATA_PICKLE = "./data/training_data.pkl"
TRAINING_LABEL_PICKLE = "./data/training_label.pkl"
TESTING_DATA_PICKLE = "./data/testing_data.pkl"


def remove_outliers(training_y, training_data):
    for index, label in enumerate(training_y):
        if label[0] < 1950:
            training_y = numpy.delete(training_y, index, axis=0)
            training_data = numpy.delete(training_data, index, axis=0)


def is_data_preprocessed():
    if os.path.exists(TRAINING_DATA_PICKLE):
        print("Reading data from pickle files")
        return True
    return False


def load_data(file_path):
    with open(file_path, "rb") as f:
        return pickle.load(f)


def dump_data(file_path, data):
    with open(file_path, "wb") as f:
        pickle.dump(data, f)


def read_data():
    if is_data_preprocessed():
        training_data = load_data(TRAINING_DATA_PICKLE)
        training_labels = load_data(TRAINING_LABEL_PICKLE)
        testing_data = load_data(TESTING_DATA_PICKLE)
    else:
        print("Reading data ....")
        data = scipy.io.loadmat('./data/MSdata.mat')
        training_data = normalize_data(data['trainx'])
        testing_data = normalize_data(data['testx'])
        training_labels = data['trainy']
        remove_outliers(training_labels, training_data)
        dump_data(TRAINING_DATA_PICKLE, training_data)
        dump_data(TRAINING_LABEL_PICKLE, training_labels)
        dump_data(TESTING_DATA_PICKLE, testing_data)
    return training_data, training_labels, testing_data


def normalize_data(data):
    scaler = MinMaxScaler()
    return scaler.fit_transform(data)


def train_predict_report(train_x, train_y, test_x, test_y):
    number_of_trees = 64
    max_depth = 10
    model = RandomForestRegressor(max_depth=max_depth, n_estimators=number_of_trees)
    print("Training the model with max_dept " + str(max_depth) + " and n_estimators = " + str(number_of_trees))
    model.fit(train_x, train_y)
    print("Making prediction .......")
    prediction = model.predict(test_x)
    error = mean_absolute_error(test_y, prediction)
    print("Mean Absolute Error: " + str(error))
    return model


def predict_on_test_data(model, test_x):
    predictions = model.predict(test_x)
    with open('output.csv', 'w') as file:
        file.write("dataid,prediction")
        for i in range(0, len(predictions)):
            file.write(str(i + 1) + "," + str(predictions[i]))
            file.write('\n')


if __name__ == '__main__':
    training_data, training_labels, testing_data = read_data()
    train_x, test_x, train_y, test_y = train_test_split(training_data, training_labels, test_size=0.2, random_state=13)
    model = train_predict_report(train_x, train_y.reshape(-1), test_x, test_y.reshape(-1))
    predict_on_test_data(model, testing_data)
