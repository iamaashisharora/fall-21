Writeup for ML Kaggle Submission:


We initially tried a bunch of different regression classifiers:


1. Random Forest Regressor 
2. K Nearest Neighbors Regressor
3. AdaBoost Regressor 
4. Gradient Boosting Regressor 
5. Histogram Gradient Boosting 
6. SVM 


We also used a Deep Learning classifier and tried a bunch of different hyper - parameters:


The following were broadly the steps performed while training the deep learning model. The deep learning library fast.ai was used in for our experimentation:

1. Pre-processing steps such as Normalization, Filling NULL values etc were performed on the loaded data 
2. The data was split into a training set and validation set using a 80-20 split 
3. The deep learning network was instantiated using the following hyper -parameters: learning rate, batch size and number of nodes in the hidden layers 
4. The model was then trained for the data using 10 epochs to calculate the MAE loss 


The following different hyper-parameters were used for the training: 

1. Learning Rate 
2. Batch Size
3. Number of nodes in the hidden layer 


Then we ran a grid search using the different combinations of values of the hyper parameters. The following were the different values of each parameter tried:

1. Batch Sizes: [32,64,128,256,512,1024]
2. Learning Rate: [0.01,0.02,0.03,0.04, 0.05]
3. Size of the 2 hidden Layers = [ [500, 250], [400, 200],  [300, 150], [200, 100], [150, 75], [100, 50], [75, 40], [50, 25], [200, 200], [150, 150], [100, 100]]


By running the deep learning model for the above combinations of the hyper - parameters, the best and the most consistent results (measured using MAE Loss on the 
Validation Set) were obtained for the following combination:

1. Learning Rate = 0.03
2. Batch Size = 1024
3. Size of Hidden Layers: 200 100

For the above combination of hyper parameters, the best possible MAE loss obtained was 5.77 on the validation set. 


The deep learning model with the above mentioned hyper parameters was used to obtain the predictions on the test set, which were submitted on Kaggle to obtain 
a MAE of    on the 30% available test set for Kaggle. 





