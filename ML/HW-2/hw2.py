import numpy as np
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier


def get_data():
    train_X = np.load('./data/train.npy')
    test_X = np.load('./data/test.npy')
    train_Y = np.load('./data/trainlabels.npy')
    test_Y = np.load('./data/testlabels.npy')
    train_X = train_X.reshape(train_X.shape[0], train_X.shape[1] * train_X.shape[2])
    test_X = test_X.reshape(test_X.shape[0], test_X.shape[1] * test_X.shape[2])
    return train_X, test_X, train_Y, test_Y


def report_on_decision_tree(max_leaf_node, train_X, test_X, train_Y, test_Y):
    decision_tree = DecisionTreeClassifier(max_leaf_nodes=max_leaf_node)
    decision_tree = decision_tree.fit(train_X, train_Y)
    training_error = 1.0 - decision_tree.score(train_X, train_Y)
    test_error = 1.0 - decision_tree.score(test_X, test_Y)
    print("Max number of leaf nodes used: " + str(decision_tree.get_n_leaves()))
    print("Training error for max leaf nodes " + str(max_leaf_node) + ": " + str(training_error))
    print("Test error for max leaf nodes " + str(max_leaf_node) + ": " + str(test_error))
    return training_error, test_error


def report_on_random_forest_with_varying_forest_size(num_of_estimators, train_X, test_X, train_Y, test_Y):
    random_forest = RandomForestClassifier(n_estimators=num_of_estimators)
    random_forest = random_forest.fit(train_X, train_Y)
    training_error = 1.0 - random_forest.score(train_X, train_Y)
    test_error = 1.0 - random_forest.score(test_X, test_Y)
    print("Training error for num of estimators " + str(num_of_estimators) + ": " + str(training_error))
    print("Test error for num of estimators " + str(num_of_estimators) + ": " + str(test_error))
    return training_error, test_error


def report_on_random_forest_with_varying_max_leaf_nodes(max_leaf_nodes, train_X, test_X, train_Y, test_Y):
    random_forest = RandomForestClassifier(max_leaf_nodes=max_leaf_nodes)
    random_forest = random_forest.fit(train_X, train_Y)
    training_error = 1.0 - random_forest.score(train_X, train_Y)
    test_error = 1.0 - random_forest.score(test_X, test_Y)
    print("Training error for max leaf nodes " + str(max_leaf_nodes) + ": " + str(training_error))
    print("Test error for max leaf nodes " + str(max_leaf_nodes) + ": " + str(test_error))
    return training_error, test_error


def report_on_random_forest(max_leaf_nodes, num_of_estimatores, train_X, test_X, train_Y, test_Y):
    random_forest = RandomForestClassifier(max_leaf_nodes=max_leaf_nodes, n_estimators=num_of_estimatores)
    random_forest = random_forest.fit(train_X, train_Y)
    training_error = 1.0 - random_forest.score(train_X, train_Y)
    test_error = 1.0 - random_forest.score(test_X, test_Y)
    print("Training error for max leaf nodes " + str(max_leaf_nodes) + " and num of estimators " + str(
        num_of_estimatores) + ": " + str(training_error))
    print("Test error for max leaf nodes " + str(max_leaf_nodes) + " and num of estimators " + str(
        num_of_estimatores) + ": " + str(test_error))
    return training_error, test_error


def run_decsion_tree(train_X, test_X, train_Y, test_Y):
    max_leaf_nodes = []
    decision_tree_train_error = []
    decision_tree_test_error = []
    for i in range(1, 15):
        max_leaf_node = 2 ** i
        training_error, test_error = report_on_decision_tree(max_leaf_node, train_X, test_X, train_Y, test_Y)
        max_leaf_nodes.append(i)
        decision_tree_train_error.append(training_error)
        decision_tree_test_error.append(test_error)

    return max_leaf_nodes, decision_tree_train_error, decision_tree_test_error


def run_random_forest_with_varying_max_leaf_size(train_X, test_X, train_Y, test_Y):
    max_leaf_nodes = []
    random_forest_train_error = []
    random_forest_test_error = []
    for i in range(1, 15):
        max_leaf_node = 2 ** i
        training_error, test_error = report_on_random_forest_with_varying_max_leaf_nodes(max_leaf_node, train_X, test_X,
                                                                                         train_Y, test_Y)
        max_leaf_nodes.append(i)
        random_forest_train_error.append(training_error)
        random_forest_test_error.append(test_error)

    return max_leaf_nodes, random_forest_train_error, random_forest_test_error


def run_random_forest_with_varying_forest_size(train_X, test_X, train_Y, test_Y):
    size_of_forest = []
    random_forest_train_error = []
    random_forest_test_error = []
    for i in range(1, 11):
        num_of_estimators = 2 ** i
        training_error, test_error = report_on_random_forest_with_varying_forest_size(num_of_estimators, train_X,
                                                                                      test_X, train_Y, test_Y)
        size_of_forest.append(i)
        random_forest_train_error.append(training_error)
        random_forest_test_error.append(test_error)

    return size_of_forest, random_forest_train_error, random_forest_test_error


def run_random_forest(train_X, test_X, train_Y, test_Y):
    random_forest_train_error_phase_1 = []
    random_forest_test_error_phase_1 = []
    random_forest_train_error_phase_2 = []
    random_forest_test_error_phase_2 = []
    number_of_parameter_phase_1 = []
    number_of_parameter_phase_2 = []

    for i in range(1, 13):
        max_leaf_nodes = 2 ** i
        training_error, test_error = report_on_random_forest(max_leaf_nodes, 1, train_X, test_X, train_Y, test_Y)
        number_of_parameter_phase_1.append(max_leaf_nodes)
        random_forest_train_error_phase_1.append(training_error)
        random_forest_test_error_phase_1.append(test_error)

    for i in range(0, 8):
        num_of_estimators = 2 ** i
        max_leaf_nodes_used = 2 ** 12
        training_error, test_error = report_on_random_forest(max_leaf_nodes_used, num_of_estimators, train_X, test_X,
                                                             train_Y, test_Y)

        number_of_parameter_phase_2.append((max_leaf_nodes_used) * num_of_estimators)
        random_forest_train_error_phase_2.append(training_error)
        random_forest_test_error_phase_2.append(test_error)

    return number_of_parameter_phase_1, number_of_parameter_phase_2, random_forest_train_error_phase_1, random_forest_train_error_phase_2, random_forest_test_error_phase_1, random_forest_test_error_phase_2


def plot_loss(x_axis, train_loss, test_loss, x_label, y_label, title):
    plt.plot(x_axis, train_loss, label='train')
    plt.plot(x_axis, test_loss, label='test')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.legend()
    plt.show()


def plot_loss_for_random_forest(phase_1_parameters, phase_2_parameters, train_error_phase_1, train_error_phase_2,
                                test_error_phase_1, test_error_phase_2, x_label, y_label, title):
    plt.plot(np.log2(phase_1_parameters), train_error_phase_1, label='train: phase 1')
    plt.plot(np.log2(phase_1_parameters), test_error_phase_1, label='test: phase 1')
    plt.plot(np.log2(phase_2_parameters), train_error_phase_2, label='train: phase 2')
    plt.plot(np.log2(phase_2_parameters), test_error_phase_2, label='test: phase 2')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.legend()
    plt.show()


if __name__ == '__main__':
    train_X, test_X, train_Y, test_Y = get_data()

    # parameters_phase_1, parameters_phase_2, train_error_phase_1, train_error_phase_2, test_error_phase_1, test_error_phase_2 = run_random_forest(
    #     train_X, test_X, train_Y, test_Y)
    # plot_loss_for_random_forest(parameters_phase_1, parameters_phase_2, train_error_phase_1, train_error_phase_2,
    #                             test_error_phase_1, test_error_phase_2, "Log2(num of paramters)", "Zero One Loss",
    #                             "Train vs Test loss for Random Forest")

    size_of_forest, random_forest_train_error, random_forest_test_error = run_random_forest_with_varying_forest_size(
        train_X, test_X, train_Y, test_Y)
    plot_loss(size_of_forest, random_forest_train_error, random_forest_test_error,
              "Log2(num of estimators)", "Zero One Loss", "Train vs Test loss for Random Forest")
