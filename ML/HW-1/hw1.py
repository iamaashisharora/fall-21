import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, classification_report


class MLEClassifier:
    def __init__(self):
        self.dimension = 0
        self.class_labels = 0
        self.data_per_class = {}

    def __fit(self, x, y):
        self.dimension = x.shape[1]
        self.class_labels = set(y)
        number_of_observations = x.shape[0]

        for class_label in self.class_labels:
            x_for_class_labels = np.array([x[j] for j in range(number_of_observations) if y[j] == class_label])

            mu = np.mean(x_for_class_labels, axis=0)
            sigma = np.cov(x_for_class_labels, rowvar=False) + 1e-10 * np.identity(x_for_class_labels.shape[1])
            inverse_of_sigma = np.linalg.inv(sigma)

            scaler = 1 / np.sqrt((2 * np.pi) ** self.dimension * np.linalg.det(sigma))

            self.data_per_class[class_label] = [mu, inverse_of_sigma, scaler]

    def __likihood_for_class(self, x, class_label):
        mu, inverse_of_sigma, scaler = self.data_per_class[class_label]

        return scaler * np.e ** (-0.5 * np.dot(np.dot((x - mu).T, inverse_of_sigma), x - mu))

    def __predict(self, x):
        likelihoods = [self.__likihood_for_class(x, class_label) for class_label in self.class_labels]
        return np.argmax(likelihoods)

    def fit_predict_report(self, x_train, y_train, x_test, y_test):
        self.__fit(x_train, y_train)

        predictions = [self.__predict(x) for x in x_test]

        print("MLE Classification")
        print(classification_report(predictions, y_test))
        print("Accuracy: " + str(accuracy_score(predictions, y_test)))


def get_training_and_test_data(training_file_path, test_file_path):
    training_data = pd.read_csv(training_file_path)
    test_data = pd.read_csv(test_file_path)
    features = ['age', 'sex', 'race', 'juv_fel_count', 'juv_misd_count', 'juv_other_count', 'priors_count',
                'c_charge_degree_F', 'c_charge_degree_M']
    label = 'two_year_recid'
    X_train = training_data[features]
    Y_train = training_data[label]
    X_test = test_data[features]
    Y_test = test_data[label]

    return X_train, Y_train, X_test, Y_test


if __name__ == '__main__':
    training_file_path = "./compas_dataset/propublicaTrain.csv"
    test_file_path = "./compas_dataset/propublicaTest.csv"

    X_train, Y_train, X_test, Y_test = get_training_and_test_data(training_file_path, test_file_path)

    mle_classifier = MLEClassifier()
    mle_classifier.fit_predict_report(X_train.to_numpy(), Y_train.to_numpy(), X_test.to_numpy(), Y_test.to_numpy())
