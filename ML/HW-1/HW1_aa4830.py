import numpy as np
import pandas as pd
import scipy.io
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold


class MLEClassifier:
    def __init__(self):
        self.class_labels = set()
        self.data_per_class = {}

    def __fit(self, x, y):
        dimension = x.shape[1]
        self.class_labels = set(y)
        number_of_observations = x.shape[0]

        for class_label in self.class_labels:
            x_for_class_labels = np.array([x[j] for j in range(number_of_observations) if y[j] == class_label])

            mu = np.mean(x_for_class_labels, axis=0)
            sigma = np.cov(x_for_class_labels, rowvar=False) + 1e-10 * np.identity(x_for_class_labels.shape[1])
            inverse_of_sigma = np.linalg.inv(sigma)

            scaler = 1 / np.sqrt((2 * np.pi) ** dimension * np.linalg.det(sigma))

            self.data_per_class[class_label] = [mu, inverse_of_sigma, scaler]

    def __likihood_for_class(self, x, class_label):
        mu, inverse_of_sigma, scaler = self.data_per_class[class_label]

        return scaler * np.e ** (-0.5 * np.dot(np.dot((x - mu).T, inverse_of_sigma), x - mu))

    def __predict(self, x):
        likelihoods = [self.__likihood_for_class(x, class_label) for class_label in self.class_labels]
        return np.argmax(likelihoods)

    def fit_predict_report(self, x_train, y_train, x_test, y_test):
        self.__fit(x_train, y_train)

        predictions = [self.__predict(x) for x in x_test]

        print("MLE Classification")
        print(classification_report(predictions, y_test))
        accuracy = accuracy_score(predictions, y_test)
        print("Accuracy: " + str(accuracy))
        return accuracy


class KNN:
    def __init__(self, training_x, training_label, k, metric):
        self.training_x = training_x
        self.training_label = training_label
        self.k = k
        self.distance_metric = metric

    def __get_distance(self, x):
        if self.distance_metric == 'L1':
            return self.__get_L1_distance(np.abs(x - self.training_x))
        if self.distance_metric == 'L2':
            return self.__get_L2_distance(np.abs(x - self.training_x))
        if self.distance_metric == 'Linfinity':
            return self.__get_Linfinity_distance(np.abs(x - self.training_x))

    def __get_L1_distance(self, difference):
        return np.sum(difference, axis=1)

    def __get_L2_distance(self, difference):
        return np.sqrt(np.sum(np.power(difference, 2), axis=1))

    def __get_Linfinity_distance(self, difference):
        return np.max(difference, axis=1)

    def __get_predicted_label(self, x):
        distance = self.__get_distance(x)
        distance_with_label = []
        for i in range(1, len(distance)):
            distance_with_label.append((distance[i], self.training_label[i]))
        distance_with_label.sort(key=lambda tuple: tuple[0])
        k_distances = distance_with_label[:self.k]
        k_distances_label = [dist[1] for dist in k_distances]
        return max(k_distances_label, key=k_distances_label.count)

    def predict_and_report(self, X_test, Y_test):
        predictions = []
        for x in X_test:
            predictions.append(self.__get_predicted_label(x))

        print("KNN Classification")
        print(classification_report(predictions, Y_test))
        accuracy = accuracy_score(predictions, Y_test)
        print("Accuracy: " + str(accuracy))
        return accuracy


class NB:
    def __init__(self):
        self.class_labels = set()
        self.prob_per_class_feature_label = {}
        self.class_prior_prob = {}
        self.median_age = 0
        self.median_priors_count = 0

    def __fit(self, x, y):
        self.class_labels = set(y)
        number_of_observations = x.shape[0]

        for class_label in self.class_labels:
            x_for_class_labels = np.array([x[j] for j in range(number_of_observations) if y[j] == class_label])
            self.median_age = np.median(x_for_class_labels[:, 0])
            self.median_priors_count = np.median(x_for_class_labels[:, 6])

            for row in x_for_class_labels:
                self.__categorize_age_and_prior_count(row)

            for i in range(x_for_class_labels.shape[1]):
                column = x_for_class_labels[:, i]
                feature_prob = dict()
                for entry in column:
                    if entry not in feature_prob.keys():
                        feature_prob[entry] = 0
                    feature_prob[entry] += 1 / x_for_class_labels.shape[0]

                if class_label not in self.prob_per_class_feature_label.keys():
                    self.prob_per_class_feature_label[class_label] = {}

                self.prob_per_class_feature_label[class_label][i] = feature_prob

            self.class_prior_prob[class_label] = x_for_class_labels.shape[0] / number_of_observations

    def __categorize_age_and_prior_count(self, row):
        row[0] = 1 if row[0] > self.median_age else 0
        row[6] = 1 if row[6] > self.median_priors_count else 0

    def __predict(self, X_test):
        predictions = []
        for row in X_test:
            self.__categorize_age_and_prior_count(row)
            predictions.append(self.__get_predicted_label(row))

        return predictions

    def __get_predicted_label(self, x):
        max_prob = 0
        class_predicted = 0

        for class_label in self.class_labels:
            probability = 1
            prob_per_clas_feature_label = self.prob_per_class_feature_label[class_label]
            for i in range(len(x)):
                if x[i] not in prob_per_clas_feature_label[i].keys():
                    return 0
                probability *= prob_per_clas_feature_label[i][x[i]]
            probability *= self.class_prior_prob[class_label]

            if probability > max_prob:
                max_prob = probability
                class_predicted = class_label

        return class_predicted

    def fit_predict_report(self, X_train, Y_train, X_test, Y_test):
        self.__fit(X_train, Y_train)
        predictions = self.__predict(X_test)

        print("NB Classification")
        print(classification_report(predictions, Y_test))
        accuracy = accuracy_score(predictions, Y_test)
        print("Accuracy: " + str(accuracy))
        return accuracy


def get_training_and_test_data(training_file_path, test_file_path):
    training_data = pd.read_csv(training_file_path)
    test_data = pd.read_csv(test_file_path)
    features = ['age', 'sex', 'race', 'juv_fel_count', 'juv_misd_count', 'juv_other_count', 'priors_count',
                'c_charge_degree_F', 'c_charge_degree_M']
    label = 'two_year_recid'

    X_train = training_data[features].to_numpy()
    Y_train = training_data[label].to_numpy()
    X_test = test_data[features].to_numpy()
    Y_test = test_data[label].to_numpy()

    return X_train, Y_train, X_test, Y_test


def get_compas_data():
    training_file_path = "./compas_dataset/propublicaTrain.csv"
    test_file_path = "./compas_dataset/propublicaTest.csv"
    return get_training_and_test_data(training_file_path, test_file_path)


def get_digit_data():
    digits_file = 'digits.mat'
    digits = scipy.io.loadmat(digits_file)
    X = digits['X']
    Y = digits['Y'].squeeze()
    return X, Y


if __name__ == '__main__':
    compas_X_train, compas_Y_train, compas_X_test, compas_Y_test = get_compas_data()

    print("Running Classifiers on Compas Data")
    mle_classifier = MLEClassifier()
    mle_classifier.fit_predict_report(compas_X_train, compas_Y_train, compas_X_test, compas_Y_test)

    print('-------------------------------------------')

    knn = KNN(compas_X_train, compas_Y_train, 11, 'L2')
    knn.predict_and_report(compas_X_test, compas_Y_test)

    print('-------------------------------------------')

    nb = NB()
    nb.fit_predict_report(compas_X_train, compas_Y_train, compas_X_test, compas_Y_test)

    digit_X, digit_Y = get_digit_data()
    digit_X = np.digitize(digit_X, np.linspace(0, 255, 25))
    digit_X = VarianceThreshold(100).fit_transform(digit_X)

    digit_X_train, digit_X_test, digit_Y_train, digit_Y_test = train_test_split(digit_X, digit_Y, train_size=0.7,
                                                                                random_state=10)

    print('-------------------------------------------')
    print("Running Classifiers on Digit Data")
    mle_classifier = MLEClassifier()
    mle_classifier.fit_predict_report(digit_X_train, digit_Y_train, digit_X_test, digit_Y_test)

    print('-------------------------------------------')

    knn_1 = KNN(digit_X_train, digit_Y_train, 11, 'L1')
    knn_1.predict_and_report(digit_X_test, digit_Y_test)

    print('-------------------------------------------')

    knn_2 = KNN(digit_X_train, digit_Y_train, 11, 'L2')
    knn_2.predict_and_report(digit_X_test, digit_Y_test)

    print('-------------------------------------------')

    knn_n = KNN(digit_X_train, digit_Y_train, 11, 'Linfinity')
    knn_n.predict_and_report(digit_X_test, digit_Y_test)
