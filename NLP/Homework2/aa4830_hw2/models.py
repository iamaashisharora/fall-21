"""
COMS 4705 Natural Language Processing Spring 2021
Kathy McKeown
Homework 2: Emotion Classification with Neural Networks - Models File

<Aashish Arora>
<aa4830>
"""

import torch
import torch.nn as nn
import torch.nn.functional as F

from hw2 import NUM_CLASSES, EMBEDDING_DIM


class DenseNetwork(nn.Module):
    def __init__(self, embeddings):
        super(DenseNetwork, self).__init__()
        self.embadding = torch.nn.Embedding(embeddings.shape[0], embeddings.shape[1]).from_pretrained(
            embeddings=embeddings)
        self.linear1 = torch.nn.Linear(EMBEDDING_DIM, EMBEDDING_DIM)
        self.linear2 = torch.nn.Linear(EMBEDDING_DIM, NUM_CLASSES)
        self.softmax = torch.nn.Softmax(1)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        result = self.embadding(x)
        embadding_result = result.mean(1).float()
        first_layer_output = self.relu(self.linear1(embadding_result))
        second_layer_output = self.linear2(first_layer_output)
        softmax_output = self.softmax(second_layer_output)
        return softmax_output


class RecurrentNetwork(nn.Module):
    def __init__(self, embeddings):
        super(RecurrentNetwork, self).__init__()
        self.embadding = torch.nn.Embedding(embeddings.shape[0], embeddings.shape[1]).from_pretrained(
            embeddings=embeddings)
        self.gru = torch.nn.GRU(embeddings.shape[1], EMBEDDING_DIM, num_layers=2, batch_first=True)
        self.linear = torch.nn.Linear(EMBEDDING_DIM, NUM_CLASSES)
        self.softmax = torch.nn.Softmax(1)

    def forward(self, x):
        result = self.embadding(x)
        first_layer_output, hidden_state = self.gru(result.double())
        second_layer_output = self.linear(hidden_state[1])
        softmax_output = self.softmax(second_layer_output)
        return softmax_output


# extension-grading
class ExperimentalNetwork1(nn.Module):
    def __init__(self, embeddings, n_filters, filter_sizes, output_dim, dropout):
        super(ExperimentalNetwork1, self).__init__()
        self.embedding = nn.Embedding(embeddings.shape[0], embeddings.shape[1]).from_pretrained(embeddings)
        self.convs = nn.ModuleList([
            nn.Conv2d(in_channels=1,
                      out_channels=n_filters,
                      kernel_size=(fs, embeddings.shape[1]))
            for fs in filter_sizes
        ])
        self.linear = nn.Linear(len(filter_sizes) * n_filters, output_dim)
        self.dropout = nn.Dropout(dropout)
        self.relu = nn.ReLU()

    def forward(self, x):
        embeddings = self.embedding(x).double()
        unsqueezed_embeddings = embeddings.unsqueeze(1)
        conv_output = [self.relu(conv(unsqueezed_embeddings).squeeze(3)) for conv in self.convs]
        max_poll_output = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conv_output]
        dropout = self.dropout(torch.cat(max_poll_output, dim=1))
        return self.linear(dropout)


# extension-grading
class ExperimentalNetwork2(nn.Module):
    def __init__(self, embeddings):
        super(ExperimentalNetwork2, self).__init__()
        self.embadding = torch.nn.Embedding(embeddings.shape[0], embeddings.shape[1]).from_pretrained(
            embeddings=embeddings)
        self.linear1 = torch.nn.Linear(embeddings.shape[1], embeddings.shape[1])
        self.linear2 = torch.nn.Linear(embeddings.shape[1], NUM_CLASSES)
        self.softmax = torch.nn.Softmax(1)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        result = self.embadding(x)
        embadding_result = result.mean(1).float()
        first_layer_output = self.relu(self.linear1(embadding_result))
        second_layer_output = self.linear2(first_layer_output)
        softmax_output = self.softmax(second_layer_output)
        return softmax_output
