# Aashish Arora

## Homework 1

## Usage

```bash
python3 hw2.py --model=dense
```

```bash
python3 hw2.py --model=RNN
```

```bash
python3 hw2.py --model=extension1
```

```bash
python3 hw2.py --model=extension2
```

## Additional Details
An additional lexicon file is passed in the 'data' folder to get a sentiment score for each word in the vocabulary. This sentiment score is concatenated with glove embeddings to use as an additional extension to this project. \
While running the CNN extension, I am getting the following warning. Please ignore this warning if you get it on your machine as it's machine-specific and the code runs fine.
```
[W NNPACK.cpp:79] Could not initialize NNPACK! Reason: Unsupported hardware.
```