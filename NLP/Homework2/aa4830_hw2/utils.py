"""
COMS 4705 Natural Language Processing Spring 2021
Kathy McKeown
Homework 2: Emotion Classification with Neural Networks - Utils File
"""
import csv

import nltk
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

EMBEDS_FN = 'resources/glove.twitter.27B.100d.txt'
LABEL_NAMES = ["happiness", "worry", "neutral", "sadness"]
LEXICA_FILE_NAME = "./data/connotation_lexicon_a.0.1.csv"


class EmotionDataset(Dataset):
    def __init__(self, data, word2idx=None, encoder=None, glove=None):
        if word2idx is None and glove is None:
            raise ValueError("Must pass either a predefined vocabulary or a GloVe dictionary to make a dataset.")

        self.label_enc = encoder
        self.emotion_frame = data
        self.word2idx = word2idx if word2idx is not None else get_word2idx(get_tokens(data), glove)
        self.X, self.y, self.label_enc = make_vectors(self.emotion_frame, self.word2idx, self.label_enc)

    def __len__(self):
        return len(self.emotion_frame)

    def __getitem__(self, idx):
        X = self.X[:, idx]
        y = self.y[idx]

        return X, y


def get_word2idx(data, glove):
    word2idx = {'<pad>': 0}
    for datapoint in data:
        for word in datapoint:
            if word in glove and word not in word2idx:
                word2idx[word] = len(word2idx)
    word2idx['<unk>'] = len(word2idx)

    return word2idx


def make_vectors(data, word2idx, label_enc=None):
    X = nn.utils.rnn.pad_sequence([torch.tensor([word2idx[word] if word in word2idx else word2idx['<unk>'] for word in
                                                 datapoint]) for datapoint in get_tokens(data)])
    y = data['sentiment'].to_numpy()
    if label_enc is None:
        label_enc = LabelEncoder()
        y = label_enc.fit_transform(y)
    else:
        y = label_enc.transform(y)
    return X, y, label_enc


def get_tokens(data):
    return [nltk.word_tokenize(datapoint.lower()) for datapoint in data['content'].tolist()]


def get_data(data_fn):
    df = pd.read_csv(data_fn)
    train, dev, test = np.split(df.sample(frac=1, random_state=4705), [int(.8 * len(df)), int(.9 * len(df))])

    print("Random samples from train set")
    print(train.sample(10))
    print("\n\n")

    return train, dev, test


def vectorize_data(train, dev, test, batch_size, embedding_dim):
    label_enc = LabelEncoder()
    label_enc.fit(np.array(LABEL_NAMES))
    print("Labels are encoded in the following class order:")
    print(label_enc.inverse_transform(np.arange(4)))
    print("\n\n")

    df = pd.read_csv(EMBEDS_FN, sep=" ", quoting=3, header=None, index_col=0)
    glove = {key: val.values for key, val in df.T.items()}

    train_data = EmotionDataset(train, glove=glove, encoder=label_enc)
    dev_data = EmotionDataset(dev, word2idx=train_data.word2idx, encoder=label_enc)
    test_data = EmotionDataset(test, word2idx=train_data.word2idx, encoder=label_enc)

    print("Train:", len(train_data), "\nDev:", len(dev_data), "\nTest:", len(test_data))
    print("\n\n")

    train_generator = DataLoader(dataset=train_data, batch_size=batch_size)
    dev_generator = DataLoader(dataset=dev_data, batch_size=batch_size)
    test_generator = DataLoader(dataset=test_data, batch_size=batch_size)

    embeddings = np.zeros((len(train_data.word2idx), embedding_dim))
    for word, i in train_data.word2idx.items():
        if word == '<pad>':
            pass
        elif word == '<unk>':
            embeddings[i] = glove['unk']
        else:
            embeddings[i] = glove[word]

    embeddings = torch.tensor(embeddings)

    return train_generator, dev_generator, test_generator, embeddings, train_data


# concatenate lexicon embeddings with glove embeddings
def get_embeddings_with_lexicon(train_data, embeddings):
    lexicon_dict = get_lexicon()
    lexicon_embeddings = np.zeros((len(train_data.word2idx), 1))
    get_lexicon_embeddings(lexicon_embeddings, lexicon_dict, train_data)
    lexicon_embeddings = torch.tensor(lexicon_embeddings)
    embeddings = torch.cat((embeddings, lexicon_embeddings), 1)
    return embeddings


# get lexicon embedding for each word in the vocabulary of the corpus
def get_lexicon_embeddings(lexicon_embeddings, lexicon_dict, train_data):
    for word, i in train_data.word2idx.items():
        if word not in lexicon_dict:
            lexicon_embeddings[i] = 0
        else:
            if word == '<pad>' or word == 'unk':
                lexicon_embeddings[i] = 0
            else:
                lexicon_embeddings[i] += sum(lexicon_dict[word])


# get sentiment score for each word in the connotation lexicon
def get_lexicon():
    lexicon_file_path = LEXICA_FILE_NAME
    file = open(lexicon_file_path)
    csvreader = csv.reader(file)
    lexicon = {}
    for row in csvreader:
        word = row[0].split('_')
        sentiment = get_lexicon_score(row[1])
        if word[0] not in lexicon.keys():
            lexicon[word[0]] = []
        lexicon[word[0]].append(sentiment)

    return lexicon


def get_lexicon_score(sentiment):
    if sentiment == 'negative':
        return -1
    elif sentiment == 'positive':
        return 1
    return 0
