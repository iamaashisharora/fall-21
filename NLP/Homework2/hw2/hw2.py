"""
COMS 4705 Natural Language Processing Spring 2021
Kathy McKeown
Homework 2: Emotion Classification with Neural Networks - Main File

<Aashish Arora>
<aa4830>
"""

import pickle

import numpy as np
from sklearn.metrics import f1_score
import torch
import torch.nn as nn
import torch.optim as optim

import utils
import models
import argparse

DATA_FN = './data/crowdflower_data.csv'
LABEL_NAMES = ["happiness", "worry", "neutral", "sadness"]

EMBEDDING_DIM = 100
BATCH_SIZE = 128
NUM_CLASSES = 4
LEARNING_RATE = 0.005
MAX_NUM_OF_EPOCHS = 100
USE_CUDA = torch.cuda.is_available()

FRESH_START = False
TEMP_FILE = "temporary_data.pkl"

torch.manual_seed(1)


# implementing Early Stopping mechanism which will save a model if the validation error decrease
# if the error does not decrease in two consecutive iterations, the training will stop and the model with lowest
# validation error will be returned
class EarlyStopping:
    def __init__(self, num_of_iteration):
        self.min_loss = np.Inf
        self.saved_model = None
        self.consecutive_loss_dec_count = 0
        self.num_of_iteration = num_of_iteration

    def stop_learning(self, loss, model):
        if self.min_loss > loss:
            self.min_loss = loss
            self.saved_model = model
            self.reset_consecutive_loss_dec_count()
            return False
        if self.consecutive_loss_dec_count < self.num_of_iteration:
            self.consecutive_loss_dec_count += 1
        if self.consecutive_loss_dec_count == self.num_of_iteration:
            print(
                "Loss did not improve for " + str(
                    self.num_of_iteration) + " consecutive iterations. Stopping the training.")
            return True
        return False

    def reset_consecutive_loss_dec_count(self):
        self.consecutive_loss_dec_count = 0


# for each epoch, get a batch of data, train the model, get loss and optimize the weights after each batch
def train_model(model, loss_fn, optimizer, train_generator, dev_generator, early_stopping, epochs):
    for epoch in range(epochs):
        total_dev_loss = 0
        for train_data, train_label in train_generator:
            model.zero_grad()
            train_y_pred = model(train_data)
            train_loss = loss_fn(train_y_pred, train_label)

            train_loss.backward()
            optimizer.step()

        with torch.no_grad():
            for dev_data, dev_label in dev_generator:
                test_y_pred = model(dev_data)
                dev_loss = loss_fn(test_y_pred, dev_label)
                total_dev_loss += dev_loss.item()

        print("Validation Loss for Epoch " + str(epoch) + ": " + str(total_dev_loss))

        if early_stopping.stop_learning(total_dev_loss, model):
            break

    return early_stopping.saved_model


def test_model(model, loss_fn, test_generator):
    gold = []
    predicted = []

    loss = torch.zeros(1)
    if USE_CUDA:
        loss = loss.cuda()

    model.eval()

    with torch.no_grad():
        for X_b, y_b in test_generator:
            y_pred = model(X_b)

            gold.extend(y_b.cpu().detach().numpy())
            predicted.extend(y_pred.argmax(1).cpu().detach().numpy())

            loss += loss_fn(y_pred.double(), y_b.long()).data

    print("Test loss: ")
    print(loss)
    print("F-score: ")
    print(f1_score(gold, predicted, average='macro'))


def main(args):
    if FRESH_START:
        print("Preprocessing all data from scratch....")
        train, dev, test = utils.get_data(DATA_FN)
        train_generator, dev_generator, test_generator, embeddings, train_data = utils.vectorize_data(train, dev, test,
                                                                                                      BATCH_SIZE,
                                                                                                      EMBEDDING_DIM)

        print("Saving DataLoaders and embeddings so you don't need to create them again; you can set FRESH_START to "
              "False to load them from file....")
        with open(TEMP_FILE, "wb+") as f:
            pickle.dump((train_generator, dev_generator, test_generator, embeddings, train_data), f)
    else:
        try:
            with open(TEMP_FILE, "rb") as f:
                print("Loading DataLoaders and embeddings from file....")
                train_generator, dev_generator, test_generator, embeddings, train_data = pickle.load(f)
        except FileNotFoundError:
            raise FileNotFoundError("You need to have saved your data with FRESH_START=True once in order to load it!")

    model = None
    epochs = MAX_NUM_OF_EPOCHS

    if args.model == 'dense':
        model = models.DenseNetwork(embeddings)
    elif args.model == 'RNN':
        model = models.RecurrentNetwork(embeddings)
        model = model.double()
        epochs = 5
    elif args.model == 'extension1':  # extension-grading - run CNN
        model = models.ExperimentalNetwork1(embeddings, 100, [2, 3, 4], 4, 0.1)
        model = model.double()
        epochs = 5
    elif args.model == 'extension2':  # extension-grading - run dense network with glove embeddings + lexicon
        embeddings = utils.get_embeddings_with_lexicon(train_data, embeddings)
        model = models.ExperimentalNetwork2(embeddings)

    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)
    early_stopping = EarlyStopping(2)

    model = train_model(model, loss_fn, optimizer, train_generator, dev_generator, early_stopping, epochs)
    test_model(model, loss_fn, test_generator)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', dest='model', required=True,
                        choices=["dense", "RNN", "extension1", "extension2"],
                        help='The name of the model to train and evaluate.')
    args = parser.parse_args()
    main(args)
