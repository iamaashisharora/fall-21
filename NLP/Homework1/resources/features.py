import csv
from os.path import exists

import numpy as np
import pandas as pd
from nltk import word_tokenize
from nltk.corpus import stopwords
from scipy.sparse import csr_matrix, hstack
from sklearn.feature_extraction.text import TfidfVectorizer

TRAIN_NON_RELIGIOUS_DATA_FILE = './train_non_religious_data_file.pkl'
TEST_NON_RELIGIOUS_DATA_FILE = './test_non_religious_data_file.pkl'
TRAIN_RELIGIOUS_DATA_FILE = './train_religious_data_file.pkl'
TEST_RELIGIOUS_DATA_FILE = './test_religious_data_file.pkl'
PRO_POLITICAL_IDEOLOGY_SCORE = 'pro_political_ideology_score'
PRO_RELIGIOUS_IDEOLOGY_SCORE = 'pro_religious_ideology_score'
CON_POLITICAL_IDEOLOGY_SCORE = 'con_political_ideology_score'
CON_RELIGIOUS_IDEOLOGY_SCORE = 'con_religious_ideology_score'
LEXICA_FILE_NAME = "connotation_lexicon_a.0.1.csv"
SWEAR_WORDS_FILE = "./data/swear_word.txt"
PRO_SENTIMENT_SCORE = 'pro_sentiment_score'
CON_SENTIMENT_SCORE = 'con_sentiment_score'
POLITICAL_IDEOLOGY = 'political_ideology'
RELIGIOUS_IDEOLOGY = 'religious_ideology'
PRO_DEBATE_LENGTH = 'pro_debate_length'
CON_DEBATE_LENGTH = 'con_debate_length'
PRO_SWEAR_WORDS = 'pro_swear_words'
CON_SWEAR_WORDS = 'con_swear_words'
USER_FEATURE_1 = 'user_feature_1'
USER_FEATURE_2 = 'user_feature_2'
LIG_FEATURE_1 = 'lig_feature_1'
LIG_FEATURE_2 = 'lig_feature_2'
PRO_DEBATER = 'pro_debater'
CON_DEBATER = 'con_debater'
PRO_TEXT = 'pro_text'
CON_TEXT = 'con_text'
CATEGORY = 'category'
RELIGION = 'Religion'
VOTERS = 'voters'
WINNER = 'winner'
ROUNDS = 'rounds'
INDEX = 'index'
TEXT = 'text'
SIDE = 'side'
PRO = 'Pro'
CON = 'Con'


def __get_user_data(user_file_path):
    print("Getting user data ...........")
    user_info = {}
    data = pd.read_json(user_file_path).T
    for user_data in data.iterrows():
        user_info[user_data[0]] = (user_data[1][POLITICAL_IDEOLOGY], user_data[1][RELIGIOUS_IDEOLOGY])

    return user_info


def __reading_data_from_pickle_file(is_test_file):
    if is_test_file:
        print("Reading test data from pickle file ...........")
        return pd.read_pickle(TEST_RELIGIOUS_DATA_FILE), pd.read_pickle(TEST_NON_RELIGIOUS_DATA_FILE)

    else:
        print("Reading training data from pickle file ...........")
        return pd.read_pickle(TRAIN_RELIGIOUS_DATA_FILE), pd.read_pickle(TRAIN_NON_RELIGIOUS_DATA_FILE)


def __get_debate_data(data_file_path, lexicon_dic_path, users_info, is_test_file):
    print("Getting debate data ...........")
    data = pd.read_json(data_file_path, lines=True)
    religious_debate_data = pd.DataFrame()
    non_religious_debate_data = pd.DataFrame()

    lexicon = __get_lexicon(lexicon_dic_path)

    for debate in data.iterrows():
        if debate[1][CATEGORY] == RELIGION:
            religious_debate_data = religious_debate_data.append(__get_data(debate, lexicon, debate[0], users_info),
                                                                 ignore_index=True)
        else:
            non_religious_debate_data = non_religious_debate_data.append(
                __get_data(debate, lexicon, debate[0], users_info), ignore_index=True)

    dump_data(is_test_file, non_religious_debate_data, religious_debate_data)

    return religious_debate_data, non_religious_debate_data


def __get_lexicon(lexicon_dic_path):
    lexicon_file_path = lexicon_dic_path + "/" + LEXICA_FILE_NAME
    file = open(lexicon_file_path)
    csvreader = csv.reader(file)
    lexicon = {}
    for row in csvreader:
        word = row[0].split('_')
        sentiment = __get_lexicon_score(row[1])
        if word[0] not in lexicon.keys():
            lexicon[word[0]] = []
        lexicon[word[0]].append(sentiment)

    return lexicon


def __get_lexicon_score(sentiment):
    if sentiment == 'negative':
        return -1
    elif sentiment == 'positive':
        return 1
    return 0


def __remove_stopwords(text):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(text)
    return [w for w in word_tokens if not w.lower() in stop_words]


def __get_data(debate, lexicon, index, users_info):
    winner = debate[1][WINNER]
    pro = ''
    con = ''

    for round in debate[1][ROUNDS]:
        for turn in round:
            if turn[SIDE] == PRO:
                pro += turn[TEXT].lower()
            elif turn[SIDE] == CON:
                con += turn[TEXT].lower()

    pro_list = __remove_stopwords(pro)
    con_list = __remove_stopwords(con)

    pro_sentiment_score = __get_sentiment_score(pro_list, lexicon)
    con_sentiment_score = __get_sentiment_score(con_list, lexicon)

    pro_swear_words = __get_swear_words_score(pro_list)
    con_swear_words = __get_swear_words_score(con_list)

    pro_debate_length = __get_debate_length(pro_list)
    con_debate_length = __get_debate_length(con_list)

    pro_political_ideology_score, pro_religious_ideology_score = __get_political_and_religious_ideology_score(
        debate[1][PRO_DEBATER], debate[1][VOTERS], users_info)
    con_political_ideology_score, con_religious_ideology_score = __get_political_and_religious_ideology_score(
        debate[1][CON_DEBATER], debate[1][VOTERS], users_info)

    return {INDEX: index, PRO_TEXT: pro, CON_TEXT: con, WINNER: winner, PRO_SENTIMENT_SCORE: pro_sentiment_score,
            CON_SENTIMENT_SCORE: con_sentiment_score, PRO_SWEAR_WORDS: pro_swear_words,
            CON_SWEAR_WORDS: con_swear_words, PRO_DEBATE_LENGTH: pro_debate_length,
            CON_DEBATE_LENGTH: con_debate_length, PRO_POLITICAL_IDEOLOGY_SCORE: pro_political_ideology_score,
            PRO_RELIGIOUS_IDEOLOGY_SCORE: pro_religious_ideology_score,
            CON_POLITICAL_IDEOLOGY_SCORE: con_political_ideology_score,
            CON_RELIGIOUS_IDEOLOGY_SCORE: con_religious_ideology_score}


def dump_data(is_test_file, non_religious_debate_data, religious_debate_data):
    if is_test_file:
        religious_debate_data.to_pickle(TEST_RELIGIOUS_DATA_FILE)
        non_religious_debate_data.to_pickle(TEST_NON_RELIGIOUS_DATA_FILE)
    else:
        religious_debate_data.to_pickle(TRAIN_RELIGIOUS_DATA_FILE)
        non_religious_debate_data.to_pickle(TRAIN_NON_RELIGIOUS_DATA_FILE)


def __get_political_and_religious_ideology_score(debater, voters, users_info):
    political_ideology_score = religious_ideology_score = 0
    debater_political_ideology, debater_religious_ideology = users_info[debater]
    for voter in voters:
        if voter in users_info.keys():
            voter_political_ideology, voter_religious_ideology = users_info[voter]
            if debater_political_ideology == voter_political_ideology:
                political_ideology_score += 1
            if debater_religious_ideology == voter_religious_ideology:
                religious_ideology_score += 1
    return political_ideology_score, religious_ideology_score


def __get_sentiment_score(words, lexicons):
    sentiment = 0
    for word in words:
        if word in lexicons.keys():
            sentiment += sum(lexicons[word]) / len(lexicons[word])
    return sentiment


def __get_swear_words_score(words):
    swear_words_score = 0
    swear_words = __get_swear_words()
    for word in words:
        if word in swear_words:
            swear_words_score += 1
    return swear_words_score


def __get_swear_words():
    swear_words = set()
    swear_words_file = open(SWEAR_WORDS_FILE, 'r')
    lines = swear_words_file.readlines()
    for line in lines:
        swear_words.add(line.split("\n")[0])
    return swear_words


def __get_debate_length(words):
    return len(words)


def get_religious_and_non_religious_features(training_file_path, test_file_path, user_file_path, lexicon_dic_path,
                                             model):
    if exists(TRAIN_RELIGIOUS_DATA_FILE):
        religious_debate_data, non_religious_debate_data = __reading_data_from_pickle_file(False)
        test_religious_debate_data, test_non_religious_debate_data = __reading_data_from_pickle_file(True)

    else:
        user_data = __get_user_data(user_file_path)
        religious_debate_data, non_religious_debate_data = __get_debate_data(training_file_path, lexicon_dic_path,
                                                                             user_data, False)
        test_religious_debate_data, test_non_religious_debate_data = __get_debate_data(test_file_path, lexicon_dic_path,
                                                                                       user_data, True)

    religious_train, religious_test = __get_features(religious_debate_data, test_religious_debate_data, model, 2000,
                                                     (1, 3))
    non_religious_train, non_religious_test = __get_features(non_religious_debate_data, test_non_religious_debate_data,
                                                             model, 5000, (1, 2))

    return religious_train, religious_debate_data, religious_test, test_religious_debate_data, non_religious_train, \
           non_religious_debate_data, non_religious_test, test_non_religious_debate_data


def __get_features(data_train, data_test, model, k, ngram_range):
    print("Getting features ...........")
    X_train, X_test = csr_matrix((len(data_train), 0), dtype=np.int8), csr_matrix((len(data_test), 0), dtype=np.int8)
    if "Ngram" in model:
        X_test, X_train = __get_ngrams_features(data_train, data_test, X_test, X_train, ngram_range, k)

    if "+Lex" in model:
        X_test, X_train = __add_additional_features(data_train[PRO_SENTIMENT_SCORE], data_train[CON_SENTIMENT_SCORE],
                                                    data_test[PRO_SENTIMENT_SCORE], data_test[CON_SENTIMENT_SCORE],
                                                    X_test, X_train)

    if "+Ling" in model:
        X_test, X_train = __add_additional_features(data_train[PRO_SWEAR_WORDS], data_train[CON_SWEAR_WORDS],
                                                    data_test[PRO_SWEAR_WORDS], data_test[CON_SWEAR_WORDS], X_test,
                                                    X_train)

        X_test, X_train = __add_additional_features(data_train[PRO_DEBATE_LENGTH], data_train[CON_DEBATE_LENGTH],
                                                    data_test[PRO_DEBATE_LENGTH], data_test[CON_DEBATE_LENGTH], X_test,
                                                    X_train)

    if "+User" in model:
        X_test, X_train = __add_additional_features(data_train[PRO_POLITICAL_IDEOLOGY_SCORE],
                                                    data_train[CON_POLITICAL_IDEOLOGY_SCORE],
                                                    data_test[PRO_POLITICAL_IDEOLOGY_SCORE],
                                                    data_test[CON_POLITICAL_IDEOLOGY_SCORE], X_test, X_train)

        X_test, X_train = __add_additional_features(data_train[PRO_RELIGIOUS_IDEOLOGY_SCORE],
                                                    data_train[CON_RELIGIOUS_IDEOLOGY_SCORE],
                                                    data_test[PRO_RELIGIOUS_IDEOLOGY_SCORE],
                                                    data_test[CON_RELIGIOUS_IDEOLOGY_SCORE], X_test, X_train)
    return X_train, X_test


def __add_additional_features(train_pro_feature, train_con_feature, test_pro_feature, test_con_feature, X_test,
                              X_train):
    X_train = hstack(
        [X_train, csr_matrix(train_pro_feature).T, csr_matrix(train_con_feature).T])
    X_test = hstack(
        [X_test, csr_matrix(test_pro_feature).T, csr_matrix(test_con_feature).T])
    return X_test, X_train


def __get_ngrams_features(data_train, data_test, X_test, X_train, ngram_range, k):
    pro_train_ngrams, pro_test_ngrams = __get_ngrams(data_train[PRO_TEXT], data_test[PRO_TEXT], ngram_range, k)
    con_train_ngrams, con_test_ngrams = __get_ngrams(data_train[CON_TEXT], data_test[CON_TEXT], ngram_range, k)
    X_train = hstack([X_train, pro_train_ngrams, con_train_ngrams])
    X_test = hstack([X_test, pro_test_ngrams, con_test_ngrams])
    return X_test, X_train


def __get_ngrams(data_train, data_test, ngram_range, k):
    vectorizer = TfidfVectorizer(ngram_range=ngram_range, max_features=k)
    X_train_ngrams = vectorizer.fit_transform(data_train)
    X_test_ngrams = vectorizer.transform(data_test)
    return X_train_ngrams, X_test_ngrams
