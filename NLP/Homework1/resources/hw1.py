import argparse

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, accuracy_score

from features import get_religious_and_non_religious_features, WINNER, INDEX


def fit_predict_report_model(features_train, labels_train, features_test, index, c, class_weight):
    model = LogisticRegression(C=c, class_weight=class_weight, fit_intercept=True, penalty='l2', tol=0.1,
                               solver='newton-cg')

    model.fit(features_train, labels_train)
    labels_predicted = model.predict(features_test)

    output = []
    for i in range(len(labels_predicted)):
        output.append((index[i], labels_predicted[i]))
    return output


def output_result(religious_predicted, non_religious_predicted, religious_actual, non_religious_actual, file_path):
    predicted_list = religious_predicted + non_religious_predicted
    actual = religious_actual.tolist() + non_religious_actual.tolist()
    predicted = []

    predicted_list.sort(key=lambda y: y[0])
    file = open(file_path, "w")
    for line in predicted_list:
        file.write(line[1] + "\n")
        predicted.append(line[1])

    print('Logistic Classification report:')
    print(classification_report(actual, predicted))
    print("Accuracy score: ", accuracy_score(actual, predicted))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', dest='train', required=True,
                        help='Full path to the training file')
    parser.add_argument('--test', dest='test', required=True,
                        help='Full path to the evaluation file')
    parser.add_argument('--user_data', dest='user_data', required=True,
                        help='Full path to the user data file')
    parser.add_argument('--model', dest='model', required=True,
                        choices=["Ngram", "Ngram+Lex", "Ngram+Lex+Ling", "Ngram+Lex+Ling+User"],
                        help='The name of the model to train and evaluate.')
    parser.add_argument('--lexicon_path', dest='lexicon_path', required=True,
                        help='The full path to the directory containing the lexica.'
                             ' The last folder of this path should be "lexica".')
    parser.add_argument('--outfile', dest='outfile', required=True,
                        help='Full path to the file we will write the model predictions')
    args = parser.parse_args()

    religious_train, religious_train_data, religious_test, religious_test_data, non_religious_train, \
    non_religious_train_data, non_religious_test, non_religious_test_data = get_religious_and_non_religious_features(
        args.train, args.test, args.user_data, args.lexicon_path, args.model)

    print("Training and Predicting ...........")
    religious_predicted = fit_predict_report_model(religious_train, religious_train_data[WINNER], religious_test,
                                                   religious_test_data[INDEX], 100, 'balanced')

    non_religious_predicted = fit_predict_report_model(non_religious_train, non_religious_train_data[WINNER],
                                                       non_religious_test, non_religious_test_data[INDEX], 1, None)

    output_result(religious_predicted, non_religious_predicted, religious_test_data[WINNER],
                  non_religious_test_data[WINNER], args.outfile)
