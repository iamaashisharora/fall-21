# Aashish Arora - aashish.arora@columbia.edu
## Homework 1
The following command needs to be run in order to train and test the model
```bash
python3 hw1.py --train=./data/train.jsonl --test=./data/val.jsonl --user_data=./data/users.json --model=Ngram+Lex+Ling+User --lexicon_path=./lexica --outfile=./output.txt
```
## Description
A swear word file is added in the zip folder which will be need to fetch Linguistic Features.
