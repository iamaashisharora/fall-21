import copy
import numpy as np
import re
import nltk
from gensim.models import Word2Vec
from scipy.sparse import lil_matrix
from sklearn.decomposition import TruncatedSVD


def get_data(file_path):
    tokenized_data = []
    with open(file_path) as f:
        lines = f.readlines()
    for line in lines:
        string = re.sub("[^a-zA-Z]", " ", line)
        tokenized_string = nltk.word_tokenize(string.lower())
        if len(tokenized_string) > 1:
            tokenized_data.append(tokenized_string)
    return tokenized_data


def generate_vocab(data):
    vocab = {}
    cnt = 0
    for index, line in enumerate(data):
        for word_index, word in enumerate(line):
            if word not in vocab.keys():
                vocab[word] = cnt
                cnt += 1
    return vocab


def generate_cooccurrence_matrix(vocab, data, window_size):
    matrix_dim = len(vocab)
    cooccurrence_matrix = lil_matrix((matrix_dim, matrix_dim), dtype=np.int8)
    for line in data:
        padd_line(line, window_size)
        for i in range(window_size, len(line) - window_size):
            context = get_context(i, line, window_size)
            populate_cooccurrence_matrix(context, cooccurrence_matrix, i, line, vocab)
    return cooccurrence_matrix


def populate_cooccurrence_matrix(context, cooccurrence_matrix, i, line, vocab):
    for word in context:
        if word != 'NA':
            cooccurrence_matrix[vocab[line[i]], vocab[word]] += 1


def get_context(i, line, window_size):
    return line[i - window_size:i] + line[i + 1:i + window_size + 1]


def padd_line(line, window_size):
    for i in range(window_size):
        line.insert(0, "NA")
        line.append("NA")


def generate_ppmi_matrix(vocab, cooccurrence_matrix):
    ppmi_matrix = lil_matrix((len(vocab), len(vocab)))
    row_sum = cooccurrence_matrix.sum(axis=1)
    col_sum = cooccurrence_matrix.sum(axis=0)
    matrix_sum = cooccurrence_matrix.sum()
    (row, col) = cooccurrence_matrix.nonzero()

    for i in range(0, len(row)):
        ppmi_matrix[row[i], col[i]] = max(0, np.log2(
            (cooccurrence_matrix[row[i], col[i]] * matrix_sum) / (row_sum[row[i], 0] * col_sum[0, col[i]])))
    return ppmi_matrix


def truncate_SVD(ppmi_matrix, k):
    truncated_svd = TruncatedSVD(n_components=k, random_state=13)
    truncated_svd_matrix = truncated_svd.fit_transform(ppmi_matrix)
    return truncated_svd_matrix


def get_svd_embedding_for_word(truncated_SVD, vocab):
    svd_embeddings = {}
    for word in vocab.keys():
        svd_embeddings[word] = truncated_SVD[vocab[word]]
    return svd_embeddings


def save_embedding(svd_embedding_for_word, file_path):
    with open(file_path, 'w') as f:
        embedding_per_word = ""
        for word in svd_embedding_for_word.keys():
            embeddings = svd_embedding_for_word[word]
            embedding_per_word += str(word) + " "
            embedding_per_word += " ".join(str(e) for e in embeddings)
            embedding_per_word += "\n"
        f.write(embedding_per_word)


def generate_SVD_embeddings(data):
    vocab = generate_vocab(data)
    context_window = [3, 5]
    dimension = [100, 300, 500]
    for window_size in context_window:
        print("Generating cooccurrence matrix with context window " + str(window_size))
        cooccurrence_matrix = generate_cooccurrence_matrix(vocab, copy.deepcopy(data), window_size)
        ppmi_matrix = generate_ppmi_matrix(vocab, cooccurrence_matrix)
        for dim in dimension:
            print("Generating SVD embeddings for dimension " + str(dim))
            truncated_SVD = truncate_SVD(ppmi_matrix, dim)
            svd_embedding_for_word = get_svd_embedding_for_word(truncated_SVD, vocab)
            file_path = "./svd/svd_embedding_" + str(window_size) + "_" + str(dim) + ".txt"
            save_embedding(svd_embedding_for_word, file_path)


def generate_word_2_vec_embeddings(data):
    context_window = [3, 5]
    dimension = [200, 300]
    negative_sample = [3, 5]

    for window_size in context_window:
        for dim in dimension:
            for sample in negative_sample:
                file_path = "./word2vec/word2vec_embedding_" + str(window_size) + "_" + str(dim) + "_" + str(
                    sample) + ".bin"
                print("Generating Word2Vec embeddings")
                word2vec_model = Word2Vec(data, vector_size=dim, window=window_size, min_count=10, sg=1,
                                          negative=sample)
                word2vec_model.wv.save_word2vec_format(file_path, binary=True)
    pass


if __name__ == '__main__':
    data = get_data("./data/brown.txt")
    generate_SVD_embeddings(data)
    generate_word_2_vec_embeddings(data)
