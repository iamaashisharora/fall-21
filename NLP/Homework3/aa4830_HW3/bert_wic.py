import os
import argparse
import pickle

import torch
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from transformers import BertTokenizer, BertModel

BATCH_SIZE = 128
LABELS = ['F', 'T']
TRAINING_DATA_PICKLE = "./wic/train/training_data.pkl"
TRAINING_LABEL_PICKLE = "./wic/train/training_label.pkl"
TESTING_DATA_PICKLE = "./wic/dev/dev_data.pkl"
TESTING_LABEL_PICKLE = "./wic/dev/dev_label.pkl"
USE_CUDA = torch.cuda.is_available()


def get_embeddings_for_data(test_dir, train_dir):
    if embeddings_exist():
        return get_embeddings()
    else:
        return generate_embeddings(test_dir, train_dir)


def get_embeddings():
    training_data = load_data(TRAINING_DATA_PICKLE)
    training_label = load_data(TRAINING_LABEL_PICKLE)
    testing_data = load_data(TESTING_DATA_PICKLE)
    testing_label = load_data(TESTING_LABEL_PICKLE)
    return training_data, training_label, testing_data, testing_label


def generate_embeddings(test_dir, train_dir):
    tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
    model = BertModel.from_pretrained('bert-base-cased')
    training_data, training_label, testing_data, testing_label = ([] for i in range(4))
    training_wic = get_wic_subset(train_dir)
    testing_wic = get_wic_subset(test_dir)
    print("Generating embedding for training data")
    generate_bert_embeddings(tokenizer, model, training_data, training_label, training_wic)
    print("Generating embedding for test data")
    generate_bert_embeddings(tokenizer, model, testing_data, testing_label, testing_wic)
    dump_data(TRAINING_DATA_PICKLE, training_data)
    dump_data(TRAINING_LABEL_PICKLE, training_label)
    dump_data(TESTING_DATA_PICKLE, testing_data)
    dump_data(TESTING_LABEL_PICKLE, testing_label)
    return training_data, training_label, testing_data, testing_label


def get_wic_subset(data_dir):
    wic = []
    split = data_dir.strip().split('/')[-1]
    with open(os.path.join(data_dir, '%s.data.txt' % split), 'r', encoding='utf-8') as datafile, \
            open(os.path.join(data_dir, '%s.gold.txt' % split), 'r', encoding='utf-8') as labelfile:
        for (data_line, label_line) in zip(datafile.readlines(), labelfile.readlines()):
            word, _, word_indices, sentence1, sentence2 = data_line.strip().split('\t')
            sentence1_word_index, sentence2_word_index = word_indices.split('-')
            label = LABELS.index(label_line.strip())
            wic.append({
                'word': word,
                'sentence1_word_index': int(sentence1_word_index),
                'sentence2_word_index': int(sentence2_word_index),
                'sentence1': sentence1,
                'sentence2': sentence2,
                'label': label
            })
    sorted_wic = sorted(wic, key=lambda d: len(d['sentence1'] + d['sentence2']))
    return sorted_wic


def generate_bert_embeddings(tokenizer, model, data, label, wic):
    i = 1
    for example in wic:
        print("Getting BERT embedding for " + str(i) + " sentence")
        data.append(get_bert_embeddings(tokenizer, model, [[example['sentence1'], example['sentence2']]]))
        label.append(example['label'])
        i += 1


def get_bert_embeddings(tokenizer, model, sentence):
    tokenized = tokenizer(sentence, padding=True, return_tensors='pt')
    model.eval()
    with torch.no_grad():
        output = model(**tokenized)
    return torch.mean(output.last_hidden_state, dim=1).numpy().reshape(-1)


def embeddings_exist():
    if os.path.exists(TRAINING_DATA_PICKLE):
        print("Reading data from pickle files")
        return True
    return False


def load_data(file_path):
    with open(file_path, "rb") as f:
        return pickle.load(f)


def dump_data(file_path, data):
    with open(file_path, "wb") as f:
        pickle.dump(data, f)


def train_predict_report(training_data, training_label, testing_data, testing_label):
    lr = LogisticRegression(solver='liblinear')
    lr.fit(training_data, training_label)
    predicted_label = lr.predict(testing_data)
    print("BERT WiC Accuracy: " + str(accuracy_score(testing_label, predicted_label)))
    return predicted_label


def classify_word_in_context(train_dir, test_dir):
    training_data, training_label, testing_data, testing_label = get_embeddings_for_data(test_dir, train_dir)
    return train_predict_report(training_data, training_label, testing_data, testing_label)


def write_to_file(predictions, file_path):
    with open(file_path, 'w') as f:
        for prediction in predictions:
            f.write(LABELS[prediction] + "\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Train a classifier to recognize words in context (WiC).'
    )
    parser.add_argument(
        '--train-dir',
        dest='train_dir',
        required=True,
        help='The absolute path to the directory containing the WiC train files.'
    )
    parser.add_argument(
        '--eval-dir',
        dest='eval_dir',
        required=True,
        help='The absolute path to the directory containing the WiC eval files.'
    )
    parser.add_argument(
        '--out-file',
        dest='out_file',
        required=True,
        help='The absolute path to the file where evaluation predictions will be written.'
    )
    args = parser.parse_args()
    prediction = classify_word_in_context(args.train_dir, args.eval_dir)
    write_to_file(prediction, args.out_file)
