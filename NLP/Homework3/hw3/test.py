def evaluate(decoder):
    bleu_scores = []
    image_caption_model.eval()
    for d_image, d_enc in zip(dev_list, enc_dev):
        bleu_scores.append(bleu(
            decoder(d_enc),
            descriptions[d_image],
            [1 / 4, 1 / 4, 1 / 4, 1 / 4]
        ))
    return np.mean(bleu_scores)


for (name, decoder) in [
    ('greedy',
     partial(image_greedy_decoder, word_to_id, id_to_word,
             image_caption_model, MAX_LEN, device,
             return_scores=False)),
    ('beam-3',
     partial(image_beam_decoder, word_to_id, id_to_word,
             image_caption_model, MAX_LEN, device, 3)),
    ('beam-5',
     partial(image_beam_decoder, word_to_id, id_to_word,
             image_caption_model, MAX_LEN, device, 5)),
    ('nucleus-0.95',
     partial(image_nucleus_decoder, word_to_id, id_to_word,
             image_caption_model, MAX_LEN, device, 0.95,
             return_scores=False))
]:
    print(name, evaluate(decoder))
