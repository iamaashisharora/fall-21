import matplotlib.pyplot as plt
import torch
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score

EMBEDDINGS = 'embeddings'
POS_TAGS = 'pos_labels'
NER_TAGS = 'ner_labels'
REL_TAGS = 'rel_label'
HIDDEN_STATE = 'hidden_states'
WORD_TOKEN_INDICES = 'word_token_indices'
ENTITY_1 = 'entity1_representations'
ENTITY_2 = 'entity2_representations'
NUM_OF_HIDDEN_STATES = 13


def read_data(file_path):
    return torch.load(file_path)


def get_embeddings_pos_ner(sentence, embeddings):
    for i in range(len(sentence[WORD_TOKEN_INDICES])):
        embeddings[POS_TAGS].append(sentence[POS_TAGS][i])
        embeddings[NER_TAGS].append(sentence[NER_TAGS][i])
        for j in range(0, NUM_OF_HIDDEN_STATES):
            embeddings['hidden_state_' + str(j)].append(np.mean(
                [sentence[HIDDEN_STATE][j][k].numpy() for k in sentence[WORD_TOKEN_INDICES][i]], axis=0))


def get_embeddings(data):
    embeddings = {}
    embeddings[POS_TAGS] = []
    embeddings[NER_TAGS] = []
    for i in range(0, NUM_OF_HIDDEN_STATES):
        embeddings['hidden_state_' + str(i)] = []
    for sentence in data:
        get_embeddings_pos_ner(sentence, embeddings)
    return embeddings


def train_test_predict_pos_ner(training_data, validation_data, i):
    training_embeddings = training_data['hidden_state_' + str(i)]
    training_pos = training_data[POS_TAGS]
    training_ner = training_data[NER_TAGS]
    validation_embeddings = validation_data['hidden_state_' + str(i)]
    validation_pos = validation_data[POS_TAGS]
    validation_ner = validation_data[NER_TAGS]

    pos_model = LogisticRegression(solver="liblinear")
    ner_model = LogisticRegression(solver="liblinear")

    pos_model.fit(training_embeddings, training_pos)
    ner_model.fit(training_embeddings, training_ner)

    pos_prediction = pos_model.predict(validation_embeddings)
    ner_prediction = ner_model.predict(validation_embeddings)

    return (f1_score(validation_pos, pos_prediction, average="macro"),
            f1_score(validation_ner, ner_prediction, average="macro"))


def remove_examples_from_validation_set(validation_data, training_data):
    for index, x in enumerate(validation_data[POS_TAGS]):
        pos_tag = validation_data[POS_TAGS][index]
        ner_tag = validation_data[NER_TAGS][index]
        if pos_tag not in training_data[POS_TAGS]:
            remove_example(index, validation_data)

        if ner_tag not in training_data[NER_TAGS]:
            remove_example(index, validation_data)


def remove_example(index, validation_data):
    validation_data[POS_TAGS].pop(index)
    validation_data[NER_TAGS].pop(index)
    for i in range(NUM_OF_HIDDEN_STATES):
        validation_data['hidden_state_' + str(i)].pop(index)


def pos_ner_classifier(data):
    print("Generating training data")
    training_data = get_embeddings(data['train'])
    print("Generating validation data")
    validation_data = get_embeddings(data['validation'])
    remove_examples_from_validation_set(validation_data, training_data)

    pos_f1_score = []
    ner_f1_score = []
    for i in range(0, NUM_OF_HIDDEN_STATES):
        print("Generating F1 score for Hidden state " + str(i))
        pos_f1, ner_f1 = train_test_predict_pos_ner(training_data, validation_data, i)
        print("POS F1 Score - " + str(pos_f1) + "\nNER F1 Score - " + str(ner_f1))
        pos_f1_score.append(pos_f1)
        ner_f1_score.append(ner_f1)

    return pos_f1_score, ner_f1_score


def get_embeddings_rel(sentence, embeddings):
    embeddings[REL_TAGS].append(sentence[REL_TAGS])
    for i in range(0, NUM_OF_HIDDEN_STATES):
        embeddings['hidden_state_' + str(i)].append(
            np.concatenate((sentence[ENTITY_1][i].numpy(), sentence[ENTITY_2][i].numpy()), axis=None))


def get_relation_embeddings(data):
    embeddings = {}
    embeddings[REL_TAGS] = []
    for i in range(0, NUM_OF_HIDDEN_STATES):
        embeddings['hidden_state_' + str(i)] = []
    for sentence in data:
        get_embeddings_rel(sentence, embeddings)
    return embeddings

    pass


def train_test_predict_rel(training_data, test_data, i):
    training_embeddings = training_data['hidden_state_' + str(i)]
    training_rel = training_data[REL_TAGS]
    test_embeddings = test_data['hidden_state_' + str(i)]
    test_rel = test_data[REL_TAGS]

    rel_model = LogisticRegression(solver="liblinear")
    rel_model.fit(training_embeddings, training_rel)
    pos_prediction = rel_model.predict(test_embeddings)
    return f1_score(test_rel, pos_prediction, average="macro")


def relation_classifier(data):
    print("Generating training data")
    training_data = get_relation_embeddings(data['train'])
    print("Generating test data")
    test_data = get_relation_embeddings(data['test'])

    rel_f1_score = []
    for i in range(0, NUM_OF_HIDDEN_STATES):
        print("Generating F1 score for Hidden state " + str(i))
        rel_f1 = train_test_predict_rel(training_data, test_data, i)
        print("REL F1 Score - " + str(rel_f1))
        rel_f1_score.append(rel_f1)

    return rel_f1_score


def plot(pos_f1_score, ner_f1_score, relation_f1_score):
    x = range(NUM_OF_HIDDEN_STATES)
    plt.plot(x, pos_f1_score, label='POS')
    plt.plot(x, ner_f1_score, label='NER')
    plt.plot(x, relation_f1_score, label='Relation')
    plt.xlabel("Hidden Layer")
    plt.ylabel("F1 Score")
    plt.title("F1 Score per Hidden Layer")
    plt.show()


if __name__ == '__main__':
    conll_data = read_data("./data/conll.pt")
    semeval_data = read_data("./data/semeval.pt")
    pos_f1_score, ner_f1_score = pos_ner_classifier(conll_data)
    relation_f1_score = relation_classifier(semeval_data)
    plot(pos_f1_score, ner_f1_score, relation_f1_score)
