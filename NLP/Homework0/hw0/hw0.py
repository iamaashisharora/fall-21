"""Build a sentiment analysis model

Sentiment analysis can be modeled as a binary text classification problem.
Here, we fit a linear classifier on features extracted from movie reviews
in order to predict whether the opinion expressed by the author is positive or negative.

"""
# Author: Olivier Grisel <olivier.grisel@ensta.org>
# License: Simplified BSD
# Modified for Columbia's COMS4705 by
# Elsbeth Turcan <eturcan@cs.columbia.edu>
# and Amith Ananthram <amith.ananthram@columbia.edu>
import nltk
import itertools
from scipy.sparse import csr_matrix, vstack
from sklearn.metrics import precision_recall_curve
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt
import numpy as np

from nltk import word_tokenize

np.random.seed(4705)


class CountVectorizer:
    def __init__(self, n_gram_range):
        self.vocabulary_ = {}
        self.n_gram_range = n_gram_range

    def fit(self, all_documents, _):
        # 1. lowercase each document
        # 2. tokenize each document using nltk's word_tokenize
        # 3. extract all n-grams (unigrams, bigrams, etc as
        #    specified by self.n_gram_range) from these tokens;
        #    represent each n-gram as a tuple (eg, ('dog',), ('cat', 'fight'))
        # 4. collect all unique n-grams across all the documents, sort
        #    them and save them as a dictionary in self.vocabulary_
        #    where the keys are the n-grams and the values are their sorted indices
        documents = lowercase_each_document(all_documents)
        tokenized_documents = tokenize_document(documents)
        n_grams = extract_n_grams(tokenized_documents, self.n_gram_range)
        dictionary = sorted_unique_n_grams(n_grams)
        self.vocabulary_ = create_vocabulary(dictionary)

        return self

    def transform(self, all_documents):
        # 1. lowercase each document
        # 2. tokenize each document using nltk's word_tokenize
        # 3. extract all n-grams (unigrams, bigrams, etc as
        #    specified by self.n_gram_range) from these tokens;
        #    represent each n-gram as a tuple (eg, ('dog',), ('cat', 'fight'))
        # 4. represent each document's n-grams as a sparse vector whose indices correspond
        #    to counts of n-grams in the sorted vocabulary_; Google "bag-of-words" if that's unclear!;
        #    if an n-gram isn't in the vocabulary_ (it wasn't in the training set), skip it!
        # 5. return a single vector of shape (# documents, # of n-grams in vocabulary)

        all_document_representations = []
        for document in all_documents:
            document_representation = np.zeros(len(self.vocabulary_), dtype=int)

            lower_case_documents = document.lower()
            tokenized_document = nltk.word_tokenize(lower_case_documents)
            self.bag_of_words(document_representation, tokenized_document)

            all_document_representations.append(csr_matrix(document_representation))

        return vstack(all_document_representations)

    def bag_of_words(self, document_representation, tokenized_document):
        for i in range(self.n_gram_range[0], self.n_gram_range[1] + 1):
            n_grams = list(zip(*[tokenized_document[j:] for j in range(i)]))
            for n_gram in n_grams:
                index = self.vocabulary_.get(n_gram)
                if index is not None:
                    document_representation[index] += 1


def lowercase_each_document(all_documents):
    return [x.lower() for x in all_documents]


def tokenize_document(documents):
    return [nltk.word_tokenize(x) for x in documents]


def extract_n_grams(tokenized_documents, n_gram_range):
    n_grams = []
    for tokens in tokenized_documents:
        for i in range(n_gram_range[0], n_gram_range[1] + 1):
            n_grams.append(list(zip(*[tokens[j:] for j in range(i)])))
    return n_grams


def sorted_unique_n_grams(n_grams):
    all_n_grams = list(itertools.chain.from_iterable(n_grams))
    return sorted(list(set(all_n_grams)))


def create_vocabulary(dictionary):
    vocab = {}
    for i in range(len(dictionary)):
        vocab[dictionary[i]] = i
    return vocab


# if the accuracy is undefined, return None
def calculate_accuracy(y_test, y_predicted):
    if len(y_test) == 0:
        return None
    return np.sum(np.equal(y_test, y_predicted)) / len(y_test)


# calculate class-based precisions, returning a tuple 
# of length 2: (class 0 precision, class 1 precision)
# if a precision is undefined, return None for that class
def calculate_recalls(y_test, y_predicted):
    if len(y_test) == 0:
        return None
    class_0_tp, class_1_tp = get_multiclass_truepositives(y_predicted, y_test)

    class_0_recall = None
    class_1_recall = None

    np_sum_0 = np.sum(np.equal(y_test, 0))
    np_sum_1 = np.sum(np.equal(y_test, 1))

    if np_sum_0 != 0:
        class_0_recall = class_0_tp / np_sum_0
    if np_sum_1 != 0:
        class_1_recall = class_1_tp / np_sum_1
    return (class_0_recall, class_1_recall)


# calculate class-based recalls, returning a tuple 
# of length 2: (class 0 recall, class 1 recall)
# if a recall is undefined, return None for that class
def calculate_precisions(y_test, y_predicted):
    if len(y_test) == 0:
        return None
    class_0_tp, class_1_tp = get_multiclass_truepositives(y_predicted, y_test)
    class_0_precision = None
    class_1_precision = None

    np_sum_0 = np.sum(np.equal(y_predicted, 0))
    np_sum_1 = np.sum(np.equal(y_predicted, 1))

    if np_sum_0 != 0:
        class_0_precision = class_0_tp / np_sum_0
    if np_sum_1 != 0:
        class_1_precision = class_1_tp / np_sum_1

    return (class_0_precision, class_1_precision)


def get_multiclass_truepositives(y_predicted, y_test):
    class_0_tp = 0
    class_1_tp = 0
    for l1, l2 in zip(y_test, y_predicted):
        if l1 == 0 and l2 == 0:
            class_0_tp += 1
        if l1 == 1 and l2 == 1:
            class_1_tp += 1
    return class_0_tp, class_1_tp


def plot_precision_recall_curve():
    precision, recall, thresholds = precision_recall_curve(y_test, y_probs)
    plt.plot(recall, precision, color='green')
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.title("Precision-Recall Curve")
    plt.show()


if __name__ == "__main__":
    # Collect the training data

    movie_reviews_data_folder = "data/txt_sentoken/"
    dataset = load_files(
        movie_reviews_data_folder, shuffle=False, encoding='utf-8')
    print("n_samples: %d" % len(dataset.data))

    # Split the dataset into training and test subsets

    docs_train, docs_test, y_train, y_test = train_test_split(
        dataset.data, dataset.target, test_size=0.25, random_state=None)

    # Build a vectorizer / classifier pipeline that uses unigrams, bigrams and trigrams

    # TODO 1/3: implement a simplified version of scikit-learn's
    # CountVectorizer above (line 28) in the manner prescribed
    clf = Pipeline([
        ('vect', CountVectorizer(n_gram_range=(1, 3))),
        ('clf', KNeighborsClassifier(n_neighbors=10)),
    ])

    # Train the classifier on the training set

    clf.fit(docs_train, y_train)

    # Predict the outcome on the testing set and store it

    y_predicted = clf.predict(docs_test)

    # Get the probabilities we'll need for the precision-recall curve

    y_probs = clf.predict_proba(docs_test)[:, 1]

    # Evaluate our model

    # TODO 2/3: implement calculate_accuracy, calculate_precisions and calculate_recalls
    # (If you're not familiar with precision or recall, please Google them!)
    print("accuracy: %0.4f" % calculate_accuracy(y_test, y_predicted))
    print("precisions: %0.4f, %0.4f" % calculate_precisions(y_test, y_predicted))
    print("recalls: %0.4f, %0.4f" % calculate_recalls(y_test, y_predicted))

    # TODO 3/3: calculate and plot the precision-recall curve
    # HINT: Take a look at scikit-learn's documentation
    # and/or find an example of this curve being plotted.
    plot_precision_recall_curve()
