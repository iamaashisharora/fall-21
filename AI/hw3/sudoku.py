import statistics
import sys
import time

ROW = "ABCDEFGHI"
COL = "123456789"


def print_board(board):
    print("-----------------")
    for i in ROW:
        row = ''
        for j in COL:
            row += (str(board[i + j]) + " ")
        print(row)


def board_to_string(board):
    ordered_vals = []
    for r in ROW:
        for c in COL:
            ordered_vals.append(str(board[r + c]))
    return ''.join(ordered_vals)


def is_solved(board):
    return not list(board.values()).__contains__(0)


def get_variable_with_minimum_remaining_values(assigned, legal_domains):
    min_remaining_values = 10
    min_remaining_values_variable = ''
    for variable in legal_domains.keys():
        if variable not in assigned.keys():
            if len(legal_domains[variable]) < min_remaining_values:
                min_remaining_values = len(legal_domains[variable])
                min_remaining_values_variable = variable
    return min_remaining_values_variable


def rollback(legal_domains, conflicting_value_indices, domain):
    for index in conflicting_value_indices:
        legal_domains[index].append(domain)


def forward_checking(mrv_variable, domain, legal_domains, row_indices, col_indices, box_indices):
    conflicting_value_indices = []
    for row_index in row_indices:
        if row_index != mrv_variable and legal_domains[row_index].__contains__(domain):
            if len(legal_domains[row_index]) == 1:
                rollback(legal_domains, conflicting_value_indices, domain)
                return False
            else:
                legal_domains[row_index].remove(domain)
                conflicting_value_indices.append(row_index)

    for col_index in col_indices:
        if col_index != mrv_variable and legal_domains[col_index].__contains__(domain):
            if len(legal_domains[col_index]) == 1:
                rollback(legal_domains, conflicting_value_indices, domain)
                return False
            else:
                legal_domains[col_index].remove(domain)
                conflicting_value_indices.append(col_index)

    for box_index in box_indices:
        if box_index != mrv_variable and legal_domains[box_index].__contains__(domain):
            if len(legal_domains[box_index]) == 1:
                rollback(legal_domains, conflicting_value_indices, domain)
                return False
            else:
                legal_domains[box_index].remove(domain)
                conflicting_value_indices.append(box_index)
    return True


def satisfy_constraints(board, mrv_variable, domain, legal_domains):
    variable_row_indices, row_values = get_row_values(board, mrv_variable)
    variable_col_indices, col_values = get_column_values(board, mrv_variable)
    variable_box_indices, box_values = get_box_values(board, mrv_variable)
    satisfy_constraints = domain not in row_values and domain not in col_values and domain not in box_values
    if satisfy_constraints:
        forward_checking(mrv_variable, domain, legal_domains, variable_row_indices, variable_col_indices,
                         variable_box_indices)
        return True
    return False


def backtrack(assignment, legal_domains, board):
    if is_solved(board):
        return board
    mrv_variable = get_variable_with_minimum_remaining_values(assignment, legal_domains)
    for domain in legal_domains[mrv_variable]:  # TODO replace this with Least Constraining Value
        if satisfy_constraints(board, mrv_variable, domain, legal_domains):
            assignment[mrv_variable] = domain  # TODO add forward checking
            board[mrv_variable] = domain
            result = backtrack(assignment, get_legal_domains(board), board)
            if result != 'Failure':
                return board
            else:
                assignment.pop(mrv_variable)
                board[mrv_variable] = 0
    return 'Failure'


def get_legal_domains(board):
    legal_domains = {}
    for key in board.keys():
        if board[key] != 0:
            continue
        else:
            legal_domains[key] = list(i for i in range(1, 10))
            variable_row_indices, row_values = get_row_values(board, key)
            variable_col_indices, col_values = get_column_values(board, key)
            variable_box_indices, box_values = get_box_values(board, key)
            legal_domains[key] = list(set(legal_domains[key]) - set(row_values) - set(col_values) - set(box_values))
    return legal_domains


def get_box_values(board, key):
    variable_box_indices = []
    box_values = []
    box_codd = (int(ROW.index(key[0]) / 3), int(COL.index(key[1]) / 3))
    for i in range(0, 3):
        for j in range(0, 3):
            box_index = ROW[box_codd[0] * 3 + i] + COL[box_codd[1] * 3 + j]
            if board[box_index] != 0:
                box_values.append(board[box_index])
            else:
                variable_box_indices.append(box_index)
    return variable_box_indices, box_values


def get_column_values(board, key):
    variable_col_indices = []
    col_values = []
    for r in range(9):
        col_index = ROW[r] + key[1]
        if board[col_index] != 0:
            col_values.append(board[col_index])
        else:
            variable_col_indices.append(col_index)
    return variable_col_indices, col_values
    # return list(filter(lambda col: col != 0, list(board[ROW[r] + key[1]] for r in range(9))))


def get_row_values(board, key):
    variable_row_indices = []
    row_values = []
    for c in range(9):
        row_index = key[0] + COL[c]
        if board[row_index] != 0:
            row_values.append(board[row_index])
        else:
            variable_row_indices.append(row_index)
    return variable_row_indices, row_values
    # return list(filter(lambda row: row != 0, list(board[key[0] + COL[c]] for c in range(9))))


def backtracking_search(board):
    start = time.time()
    legal_domains = get_legal_domains(board)
    solved_board = backtrack({}, legal_domains, board)
    end = time.time()
    time_taken = end - start
    return solved_board, time_taken


if __name__ == '__main__':
    time_take_per_board = []
    if len(sys.argv) > 1:

        # print(sys.argv[1]) # TODO: Comment this out when timing runs.
        board = {ROW[r] + COL[c]: int(sys.argv[1][9 * r + c])
                 for r in range(9) for c in range(9)}

        solved_board, time_taken = backtracking_search(board)
        time_take_per_board.append(time_taken)
        board_output = board_to_string(solved_board)
        # print(board_output) # TODO: Comment this out when timing runs.

        out_filename = 'output.txt'
        outfile = open(out_filename, "w")
        outfile.write(board_output)
        outfile.write('\n')

    else:
        src_filename = './starter/sudokus_start.txt'
        try:
            srcfile = open(src_filename, "r")
            sudoku_list = srcfile.read()
        except:
            print("Error reading the sudoku file %s" % src_filename)
            exit()

        out_filename = 'output.txt'
        outfile = open(out_filename, "w")

        for line in sudoku_list.split("\n"):

            if len(line) < 9:
                continue

            board = {ROW[r] + COL[c]: int(line[9 * r + c])
                     for r in range(9) for c in range(9)}

            # TODO: Comment this out when timing runs.
            # print_board(board)

            solved_board, time_taken = backtracking_search(board)
            time_take_per_board.append(time_taken)

            # TODO: Comment this out when timing runs.
            # print_board(solved_board)

            outfile.write(board_to_string(solved_board))
            outfile.write('\n')

        print("Finishing all boards in file.")

    print("Mean (in sec): " + str(statistics.mean(time_take_per_board)))
    print("Max (in sec): " + str(max(time_take_per_board)))
    print("Min (in sec): " + str(min(time_take_per_board)))
    print("Standard Deviation (in sec): " + str(statistics.stdev(time_take_per_board)))
