import numpy as np


def p1(k: int) -> str:
    result = ""
    fact = get_factorial(k)
    result += str(fact)
    for i in range(k, 1, -1):
        fact /= i
        result += "," + str(int(fact))
    return result


def get_factorial(k):
    fact = 1
    for i in range(1, k + 1):
        fact *= i
    return fact


def p2_a(x: list, y: list) -> list:
    return sorted(y, reverse=True)[:-1]


def p2_b(x: list, y: list) -> list:
    x.reverse()
    return x


def p2_c(x: list, y: list) -> list:
    return sorted(list(set(x + y)))


def p2_d(x: list, y: list) -> list:
    return [x, y]


def p3_a(x: set, y: set, z: set) -> set:
    return x.union(y, z)


def p3_b(x: set, y: set, z: set) -> set:
    return x.intersection(y, z)


def p3_c(x: set, y: set, z: set) -> set:
    combined_set = [x, y, z]
    unique_elements = set()
    for _ in combined_set:
        unique_elements |= combined_set[0] - set.union(*combined_set[1:])
        combined_set = combined_set[-1:] + combined_set[:-1]
    return unique_elements


def p4_a() -> np.array:
    chess_board = np.zeros((5, 5), dtype=int)
    chess_board[:, 0] = 1
    chess_board[:, 4] = 1
    chess_board[0, :] = 1
    chess_board[4, :] = 1
    chess_board[2, 2] = 2
    return chess_board


def p4_b(x: np.array) -> list:
    result = []
    pawn_location = np.where(x == 2)
    pawn = (pawn_location[0][0], pawn_location[1][0])
    knights_location = np.where(x == 1)
    knights = get_knigts(knights_location)
    for knight in knights:
        target_locations = get_target_location(knight)
        if pawn in target_locations:
            result.append(knight)
    return result


def get_knigts(knights_location):
    knights = []
    number_of_knights = len(knights_location[0])
    for i in range(0, number_of_knights):
        knights.append((knights_location[0][i], knights_location[1][i]))
    return knights


def get_target_location(knight):
    return [
        (knight[0] + 2, knight[1] + 1),
        (knight[0] + 2, knight[1] - 1),
        (knight[0] - 2, knight[1] + 1),
        (knight[0] - 2, knight[1] - 1),
        (knight[0] + 1, knight[1] + 2),
        (knight[0] - 1, knight[1] + 2),
        (knight[0] + 1, knight[1] - 2),
        (knight[0] - 1, knight[1] - 2)
    ]


def p5_a(x: dict) -> int:
    count = 0
    for key in x:
        if len(x[key]) == 0:
            count += 1
    return count


def p5_b(x: dict) -> int:
    count = 0
    for key in x:
        if len(x[key]) != 0:
            count += 1
    return count


def p5_c(x: dict) -> list:
    result = []
    for key in x:
        if len(x[key]) != 0:
            values = x[key]
            for value in values:
                if (key, value) not in result and (value, key) not in result:
                    result.append((key, value))
    return result


def p5_d(x: dict) -> np.array:
    adj_matrix = np.zeros((7, 7), dtype=int)
    for key in x:
        if len(x[key]) != 0:
            values = x[key]
            for value in values:
                key_index, value_index = get_index(key, value)
                adj_matrix[key_index, value_index] = 1
    return adj_matrix


def get_index(key, value):
    key_index = ord(key.lower()) - 97
    value_index = ord(value.lower()) - 97
    return key_index, value_index


class PriorityQueue(object):
    def __init__(self):
        self.priority = {'apple': 3, 'banana': 4, 'carrot': 5, 'kiwi': 2, 'orange': 3, 'mango': 1, 'pineapple': 1}
        self.queue = {1: [], 2: [], 3: [], 4: [], 5: []}

    def push(self, x):
        priority = self.priority[x]
        self.queue[priority].append(x)

    def pop(self):
        values = self.queue.values()
        for value in values:
            if len(value) != 0:
                pop_value = value[0]
                value.remove(value[0])
                return pop_value

    def is_empty(self):
        is_empty = True
        values = self.queue.values()
        for value in values:
            if len(value) != 0:
                is_empty = False
                break
        return is_empty


if __name__ == '__main__':
    print(p1(k=8))
    print('-----------------------------')
    print(p2_a(x=[], y=[1, 3, 5]))
    print(p2_b(x=[2, 4, 6], y=[]))
    print(p2_c(x=[1, 3, 5, 7], y=[1, 2, 5, 6]))
    print(p2_d(x=[1, 3, 5, 7], y=[1, 2, 5, 6]))
    print('------------------------------')
    print(p3_a(x={1, 3, 5, 7}, y={1, 2, 5, 6}, z={7, 8, 9, 1}))
    print(p3_b(x={1, 3, 5, 7}, y={1, 2, 5, 6}, z={7, 8, 9, 1}))
    print(p3_c(x={1, 3, 5, 7}, y={1, 2, 5, 6}, z={7, 8, 9, 1}))
    print('------------------------------')
    print(p4_a())
    print(p4_b(p4_a()))
    print('------------------------------')
    graph = {
        'A': ['D', 'E'],
        'B': ['E', 'F'],
        'C': ['E'],
        'D': ['A', 'E'],
        'E': ['A', 'B', 'C', 'D'],
        'F': ['B'],
        'G': []
    }
    print(p5_a(graph))
    print(p5_b(graph))
    print(p5_c(graph))
    print(p5_d(graph))
    print('------------------------------')
    pq = PriorityQueue()
    pq.push('apple')
    pq.push('kiwi')
    pq.push('orange')
    while not pq.is_empty():
        print(pq.pop())
