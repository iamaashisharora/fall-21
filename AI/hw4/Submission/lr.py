import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
from matplotlib import cm

def normalise(data):
    return (data - np.mean(data))/np.std(data)

def normalise_data(df):
    age = np.array(df.iloc[:, 0])
    weight = np.array(df.iloc[:, 1])
    height = np.array(df.iloc[:, 2])
    update_age = normalise(age)
    update_weight = normalise(weight)
    return np.stack((np.ones(len(update_age)), update_age, update_weight), axis = -1), height

def stochastic_gradient(weights, alpha, data, label):
    error = [0]*3
    for i, data_sample in enumerate(data):
        predicted = np.dot(weights, data_sample)
        for j in range(3):
            error[j] += ((predicted - label[i]) * data_sample[j])
    for j in range(3):
        weights[j] -= (alpha*error[j]/len(data))
    return weights

def linear_regression(num_epochs, alpha, data, label):
    weights = [0]*3
    for epoch in range(num_epochs):
        weights = stochastic_gradient(weights, alpha, data, label)
    return weights

def visualize_3d(df, lin_reg_weights, alpha=0., xlabel='age', ylabel='weight', zlabel='height',title=''):

    ax = plt.figure().gca(projection='3d')

    ax.scatter(df[0], df[1], df[2])

    axes1 = np.arange(0, max(df[0]), step=.05)  # age
    axes2 = np.arange(0, max(df[1]), step=.05)  # weight
    axes1, axes2 = np.meshgrid(axes1, axes2)
    axes3 = np.array([lin_reg_weights[0] +
                      lin_reg_weights[1] * f1 +
                      lin_reg_weights[2] * f2  # height
                      for f1, f2 in zip(axes1, axes2)])
    plane = ax.plot_surface(axes1, axes2, axes3, cmap=cm.Spectral)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)

    if title == '':
        title = 'LinReg Height with Alpha %f' % alpha
    ax.set_title(title)
    plt.show()

def write_file(filename, output):
    f = open(filename, 'w+')
    f.write(output)

def main():
    df = pd.read_csv(sys.argv[1], header=None)
    feature, height = normalise_data(df)
    alpha_rate = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 0.75]
    output = ""
    for alpha in alpha_rate:
        num_epoch = 100
        weight = linear_regression(num_epoch, alpha, feature, height)
        result = str(alpha) + "," + str(num_epoch) + "," + str(weight[0]) + "," + str(weight[1]) + "," + str(weight[2]) + "\n"
        output+=result
        write_file(sys.argv[2], output)
        visualize_3d(df, weight, alpha=alpha)



if __name__ == "__main__":
    main()
