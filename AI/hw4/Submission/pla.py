import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
from matplotlib import cm
import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import Axes3D

def f(row, weights):
    predicted = 0
    for i in range(len(weights)-1):
        predicted += weights[i]*row[i]
    predicted +=weights[2]
    return 1 if (predicted>0) else -1


def pla(df):
    weights = [0 for i in range(3)]
    result = []
    while(True):
        flag = False
        for i, row in df.iterrows():
            predicted = f(row, weights)
            label = df.iloc[i,2]
            if(label*predicted <=0):
                flag = True
                for j in range(len(weights)-1):
                    weights[j] = weights[j] + (label*row[j])
                weights[2] = weights[2] + label
        result.append(weights.copy())
        if(flag == False):
            break
    return result


def main():
    df = pd.read_csv(sys.argv[1], header = None)
    result_weight = pla(df)
    outfile = sys.argv[2]
    f = open(outfile, 'w+')
    for w in result_weight:
        w = [str(i) for i in w]
        result = ','.join(w)
        result+="\n"
        f.write(result)
    visualize_scatter(df, weights = result_weight[len(result_weight)-1])

    '''YOUR CODE GOES HERE'''

def visualize_scatter(df, feat1=0, feat2=1, labels=2, weights=[-1, -1, 1],
                      title=''):
    colors = pd.Series(['r' if label > 0 else 'b' for label in df[labels]])
    ax = df.plot(x=feat1, y=feat2, kind='scatter', c=colors)
    xmin, xmax = ax.get_xlim()


    a = weights[0]
    b = weights[1]
    c = weights[2]

    def y(x):
        return (-a/b)*x - c/b

    line_start = (xmin, xmax)
    line_end = (y(xmin), y(xmax))
    line = mlines.Line2D(line_start, line_end, color='red')
    ax.add_line(line)


    if title == '':
        title = 'Scatter of feature %s vs %s' %(str(feat1), str(feat2))
    ax.set_title(title)
    plt.show()

if __name__ == "__main__":
    """DO NOT MODIFY"""
    main()