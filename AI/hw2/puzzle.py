from __future__ import division
from __future__ import print_function

import heapq
import resource
import sys
import math
import time
from collections import deque


class QueueFrontier:
    def __init__(self):
        self.queue_frontier = deque()
        self.queue_set = set()

    def in_frontier(self, config_value):
        if config_value in self.queue_set:
            return True
        else:
            return False

    def push(self, object, config_value):
        self.queue_set.add(config_value)
        self.queue_frontier.append(object)

    def pop(self):
        return self.queue_frontier.popleft()


class StackFrontier:
    def __init__(self):
        self.stack_frontier = []
        self.stack_set = set()

    def in_frontier(self, config_value):
        if config_value in self.stack_set:
            return True
        else:
            return False

    def push(self, object, config_value):
        self.stack_set.add(config_value)
        self.stack_frontier.append(object)

    def pop(self):
        return self.stack_frontier.pop()


class PuzzleState(object):
    def __init__(self, config, n, parent=None, action="Initial", cost=0, depth=0):
        if n * n != len(config) or n < 2:
            raise Exception("The length of config is not correct!")
        if set(config) != set(range(n * n)):
            raise Exception("Config contains invalid/duplicate entries : ", config)

        self.n = n
        self.cost = cost
        self.parent = parent
        self.action = action
        self.config = config
        self.children = []
        self.depth = depth

        self.blank_index = self.config.index(0)

    def display(self):
        for i in range(self.n):
            print(self.config[3 * i: 3 * (i + 1)])

    def move_up(self):
        if self.blank_index - 3 >= 0:
            child_config = swap_tiles(self.config, self.blank_index, self.blank_index - 3)
            return PuzzleState(child_config, self.n, self, "Up", self.cost, self.depth + 1)
        return None

    def move_down(self):
        if self.blank_index + 3 <= 8:
            child_config = swap_tiles(self.config, self.blank_index, self.blank_index + 3)
            return PuzzleState(child_config, self.n, self, "Down", self.cost, self.depth + 1)
        return None

    def move_left(self):
        if self.blank_index not in [0, 3, 6]:
            child_config = swap_tiles(self.config, self.blank_index, self.blank_index - 1)
            return PuzzleState(child_config, self.n, self, "Left", self.cost, self.depth + 1)
        return None

    def move_right(self):
        if self.blank_index not in [2, 5, 8]:
            child_config = swap_tiles(self.config, self.blank_index, self.blank_index + 1)
            return PuzzleState(child_config, self.n, self, "Right", self.cost, self.depth + 1)
        return None

    def expand(self):
        if len(self.children) != 0:
            return self.children
        self.cost += 1

        children = [
            self.move_up(),
            self.move_down(),
            self.move_left(),
            self.move_right()]

        self.children = [state for state in children if state is not None]
        return self.children

    def __lt__(self, other):
        actions_priority = ['Up', 'Down', 'Left', 'Right']
        if self.action == other.action:
            return True
        return actions_priority.index(self.action) < actions_priority.index(other.action)


def writeOutput(output):
    with open("./output.txt", 'a') as file:
        file.write(output)


def generate_output(max_search_depth, nodes_expanded, state, start_time, start_mem):
    output = ""
    output += path_to_goal(state) + "\n"
    output += cost_to_path(state) + "\n"
    output += "nodes_expanded: " + str(nodes_expanded) + "\n"
    output += search_depth(state) + "\n"
    output += "max_search_depth: " + str(max_search_depth) + "\n"

    end_men = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    end_time = time.time()
    output += "running_time: " + str(end_time - start_time) + "\n"
    output += "max_mem_usage: " + str((end_men - start_mem) / (1024 * 1024)) + "\n \n"
    writeOutput(output)


def bfs_search(initial_state):
    start_time = time.time()
    start_mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    frontier = QueueFrontier()
    visited = set()
    frontier.push(initial_state, tuple(initial_state.config))
    nodes_expanded = 0
    max_search_depth = 0

    while frontier.queue_frontier:
        state = frontier.pop()
        visited.add(tuple(state.config))

        if test_goal(state):
            generate_output(max_search_depth, nodes_expanded, state, start_time, start_mem)
            return

        children = state.expand()
        nodes_expanded += 1
        for child in children:
            max_search_depth = max(max_search_depth, child.depth)
            config_value = tuple(child.config)
            if not frontier.in_frontier(config_value) and config_value not in visited:
                frontier.push(child, config_value)


def dfs_search(initial_state):
    start_time = time.time()
    start_men = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    frontier = StackFrontier()
    visited = set()
    frontier.push(initial_state, tuple(initial_state.config))
    nodes_expanded = 0
    max_search_depth = 0

    while frontier.stack_frontier:
        state = frontier.pop()
        visited.add(tuple(state.config))

        if test_goal(state):
            generate_output(max_search_depth, nodes_expanded, state, start_time, start_men)
            return

        children = reversed(state.expand())
        nodes_expanded += 1
        for child in children:
            max_search_depth = max(max_search_depth, child.depth)
            config_value = tuple(child.config)
            if not frontier.in_frontier(config_value) and config_value not in visited:
                frontier.push(child, config_value)


def A_star_search(initial_state):
    start_time = time.time()
    start_men = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    heap_queue = []
    heapq.heappush(heap_queue, (calculate_total_cost(initial_state.config, initial_state.n, 0), initial_state))
    visited = set()
    nodes_expanded = 0
    max_search_depth = 0

    while heap_queue:
        state = heapq.heappop(heap_queue)
        visited.add(tuple(state[1].config))

        if test_goal(state[1]):
            generate_output(max_search_depth, nodes_expanded, state[1], start_time, start_men)
            return

        children = state[1].expand()
        nodes_expanded += 1
        for child in children:
            max_search_depth = max(max_search_depth, child.depth)
            config_value = tuple(child.config)
            if config_value not in visited:
                heapq.heappush(heap_queue, (calculate_total_cost(child.config, child.n, child.depth), child))


def swap_tiles(config, index1, index2):
    new_config = list(config)
    new_config[index1], new_config[index2] = new_config[index2], new_config[index1]
    return new_config


def cost_to_path(state):
    return "cost_to_path: " + str(state.cost)


def path_to_goal(state):
    path = []
    while state.parent is not None:
        path.append(state.action)
        state = state.parent
    path.reverse()
    path_to_goal = ', '.join(path)
    return "path_to_goal: [" + path_to_goal + "]"


def search_depth(state):
    return "search_depth:" + str(state.depth)


def calculate_total_cost(config, n, cost_to_reach_state):
    total_cost = 0
    for i in range(len(config)):
        total_cost += calculate_manhattan_dist(i, config[i], n)
    return total_cost + cost_to_reach_state


def calculate_manhattan_dist(idx, value, n):
    row_idx = int(idx / n)
    col_idx = idx % n
    row_value = int(value / n)
    col_value = value % n

    return abs(row_idx - row_value) + abs(col_idx - col_value)


def test_goal(puzzle_state):
    """test the state is the goal state or not"""
    goal_state = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    if goal_state == puzzle_state.config:
        return True
    return False


def main():
    search_mode = sys.argv[1].lower()
    begin_state = sys.argv[2].split(",")
    begin_state = list(map(int, begin_state))
    board_size = int(math.sqrt(len(begin_state)))
    hard_state = PuzzleState(begin_state, board_size)

    if search_mode == "bfs":
        bfs_search(hard_state)
    elif search_mode == "dfs":
        dfs_search(hard_state)
    elif search_mode == "ast":
        A_star_search(hard_state)
    else:
        print("Enter valid command arguments !")


if __name__ == '__main__':
    main()
