import sys

sys.path.append("../BusinessLogic")
sys.path.append("../DB")

import os
import click

from flask import Flask, render_template, request, redirect, url_for
from sqlalchemy import create_engine

import RequestHandler

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../templates')
app = Flask(__name__, template_folder=tmpl_dir)

DATABASEURI = "postgresql://aa4830:4255@34.74.246.148/proj1part2"
engine = create_engine(DATABASEURI)
app.secret_key = 'secret'


@app.before_request
def before_request():
    RequestHandler.connect_to_DB(engine)


@app.teardown_request
def teardown_request(exception):
    RequestHandler.drop_DB_connection()


@app.route('/', methods=['GET', 'POST'])
def index():
    if RequestHandler.logged_in():
        if request.method == 'GET':
            context = RequestHandler.get_jobs()
            return render_template("index.html", **context)
        else:
            org = request.form['org']
            pos = request.form['pos']
            link = request.form['link']
            description = request.form['description']
            location = request.form['location']
            salary = request.form['salary']
            RequestHandler.add_job_posting(org, pos, link, description, location, salary)
            return redirect(url_for('index'))
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        if RequestHandler.auth_user(email):
            return redirect(url_for('index'))
    return render_template("login.html")


@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        age = request.form['age']
        dob = request.form['dob']
        work_ex = request.form['work_ex']
        skills = request.form['skills'].split(', ')
        seeking = request.form['seeking']
        giving = request.form['giving']
        RequestHandler.add_user(name, email, age, dob, work_ex, skills, seeking, giving)
        return redirect(url_for('login'))
    return render_template("sign_up.html")


@app.route('/organisations')
def organisations():
    if RequestHandler.logged_in():
        context = RequestHandler.get_organisations()
        return render_template("organisations.html", **context)
    return redirect(url_for('login'))


@app.route('/referral', methods=['GET', 'POST'])
def referral():
    if RequestHandler.logged_in():
        if request.method == 'GET':
            context = RequestHandler.get_referrals()
            return render_template("referral.html", **context)
        else:
            user = request.form['user']
            org_pos = request.form['pos']
            des = request.form['des']
            RequestHandler.add_referral(user, org_pos, des)
            return redirect(url_for('referral'))
    return redirect(url_for('login'))


@app.route('/applications', methods=['GET'])
def applications():
    if RequestHandler.logged_in():
        if request.method == 'GET':
            context = RequestHandler.get_applications()
            return render_template("application.html", **context)
    return redirect(url_for('login'))


@app.route('/profile', methods=['GET'])
def profile():
    if RequestHandler.logged_in():
        if request.method == 'GET':
            context = RequestHandler.get_user_profile()
            return render_template("profile.html", **context)
    return redirect(url_for('login'))


@app.route('/profile/<user_id>', methods=['GET'])
def user_profile(user_id):
    if RequestHandler.logged_in():
        if request.method == 'GET':
            context = RequestHandler.get_profile(user_id)
            return render_template("profile.html", **context)
    return redirect(url_for('login'))


if __name__ == "__main__":
    @click.command()
    @click.option('--debug', is_flag=True)
    @click.option('--threaded', is_flag=True)
    @click.argument('HOST', default='0.0.0.0')
    @click.argument('PORT', default=8111, type=int)
    def run(debug, threaded, host, port):
        HOST, PORT = host, port
        print("running on %s:%d" % (HOST, PORT))
        app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


    run()
