import DBLayer
from flask import session

user_session = session


def auth_user(email):
    user_info = DBLayer.get_user_info()
    for user in user_info:
        if email == user['email']:
            create_session(email, user['name'])
            return True
    return False


def get_jobs():
    jobs = DBLayer.get_all_jobs_opening()
    organisations = DBLayer.get_org_names()
    context = dict(jobs=jobs, orgs=organisations)
    return context


def connect_to_DB(engine):
    DBLayer.connect_to_DB(engine)


def drop_DB_connection():
    DBLayer.drop_DB_connection()


def get_session():
    return user_session


def logged_in():
    user_session = get_session()
    if 'email' in user_session.keys():
        return True
    return False


def create_session(email, name):
    user_session['email'] = email
    user_session['name'] = name


def filter_users(users):
    user_info = []
    session = get_session()
    email = session['email']
    for index, user in enumerate(users):
        if user['email'] == email:
            users.pop(index)
        else:
            user_info.append({'info': user['name'] + " - " + user['email']})
    return user_info


def get_org_pos_from_opening(openings):
    org_pos = []
    for opening in openings:
        org_pos.append({'pos': opening['org'] + " - " + opening['pos']})
    return org_pos


def get_referrals():
    session = get_session()
    user_id = DBLayer.get_user_id_from_email(session['email'])
    receieved_referals = DBLayer.get_received_referals(user_id)
    given_referals = DBLayer.get_given_referrals(user_id)
    users = DBLayer.get_user_info()
    openings = DBLayer.get_all_jobs_opening()
    org_pos = get_org_pos_from_opening(openings)
    users_info = filter_users(users)
    context = dict(received=receieved_referals, given=given_referals, users=users_info, org_pos=org_pos)
    return context


def get_applications():
    session = get_session()
    user_id = DBLayer.get_user_id_from_email(session['email'])
    applications = DBLayer.get_job_applications(user_id)
    context = dict(apps=applications)
    return context


def get_organisations():
    organisations = DBLayer.get_organisations()
    return dict(organisations=organisations)


def get_user_email(user_info):
    return user_info.split(" - ")[1]


def get_org_pos(org_pos):
    return org_pos.split(" - ")[0], org_pos.split(" - ")[1]


def add_referral(user_info, org_pos, des):
    sender_id = DBLayer.get_user_id_from_email(session['email'])
    receiver_id = DBLayer.get_user_id_from_email(get_user_email(user_info))
    organisation, position = get_org_pos(org_pos)
    pos_id, org_id = DBLayer.get_organisation_id_and_position_id(organisation, position)
    DBLayer.post_referral(sender_id, receiver_id, org_id, pos_id, des)


def add_job_posting(org, pos, link, description, location, salary):
    organisation_id = DBLayer.get_org_id(org)
    pid = DBLayer.get_pid_after_inserting_position(pos, description, location, salary, organisation_id)
    uid = DBLayer.get_user_id_from_email(session['email'])
    DBLayer.add_job_opening(organisation_id, pid, pos, uid, link)


def add_user(name, email, age, dob, work_ex, skills, seeking, giving):
    user_id = DBLayer.get_new_user_id()
    DBLayer.post_user(user_id, name, email, age, dob, work_ex, seeking, giving)
    for skill in skills:
        skill_id = DBLayer.add_get_skill(skill)
        DBLayer.add_skill_mapping(skill_id, user_id)


def get_user_profile():
    session = get_session()
    user_id = DBLayer.get_user_id_from_email(session['email'])
    user_details = DBLayer.get_user_profile_details(user_id)
    return user_details


def get_profile(user_id):
    user_details = DBLayer.get_user_profile_details(user_id)
    return user_details
