import traceback

from flask import g


def connect_to_DB(engine):
    try:
        g.conn = engine.connect()
    except:
        print("uh oh, problem connecting to database")
        traceback.print_exc()
        g.conn = None


def drop_DB_connection():
    try:
        g.conn.close()
    except Exception as e:
        pass


def get_new_user_id():
    count = 0
    cursor = g.conn.execute("select count(*) as c from users")
    for result in cursor:
        count = result['c']
    return count + 1


def get_new_pos_id():
    count = 0
    cursor = g.conn.execute("select max(position_id) as c from positions")
    for result in cursor:
        count = result['c']
    return count + 1


def get_new_skill_id():
    count = 0
    cursor = g.conn.execute("select max(skill_id) as c from skills")
    for result in cursor:
        count = result['c']
    return count + 1


def add_get_skill(skill):
    skill_id = None
    cursor = g.conn.execute("select skill_id from skills where name=%s", skill)
    for result in cursor:
        skill_id = result['skill_id']
    if skill_id is None:
        new_skill_id = get_new_skill_id()
        g.conn.execute("insert into skills values (%s, %s)", new_skill_id, skill)
        return new_skill_id
    else:
        return skill_id


def add_skill_mapping(skill_id, user_id):
    g.conn.execute("insert into users_skill values (%s, %s)", skill_id, user_id)


def get_user_info():
    user_info = []
    cursor = g.conn.execute("select name, email from users")
    for result in cursor:
        user_info.append({'name': result[0], 'email': result[1]})
    return user_info


def get_all_jobs_opening():
    jobs = []
    cursor = g.conn.execute(
        "select organisations.name, positions.name, positions.location, job_openings.link from organisations join job_openings on job_openings.oid = organisations. organisation_id join positions on positions.position_id = job_openings.pid;")
    for result in cursor:
        jobs.append({'org': result[0], 'pos': result[1], 'loc': result[2], 'link': result[3]})
    return jobs


def get_user_id_from_email(email):
    cursor = g.conn.execute("select user_id from users where email = %s", email)
    for result in cursor:
        return result[0]


def get_job_applications(user_id):
    apps = []
    cursor = g.conn.execute(
        "SELECT org.name as org, p.name as pos FROM jobs_applications app JOIN organisations org ON org.organisation_id=app.oid JOIN positions p ON p.position_id = app.pid WHERE app.user_id = %s",
        user_id)
    for result in cursor:
        apps.append({'pos': result['pos'], 'org': result['org']})
    return apps


def get_given_referrals(user_id):
    referals = []
    cursor = g.conn.execute(
        "SELECT u.name as user, p.name as pos, o.name as org FROM referral r JOIN users u ON r.receiver_id=u.user_id JOIN positions p ON p.position_id = r.pid JOIN organisations o ON o.organisation_id=r.oid WHERE sender_id = %s",
        user_id)
    for result in cursor:
        referals.append({'user': result['user'], 'pos': result['pos'], 'org': result['org']})
    return referals


def get_received_referals(user_id):
    referals = []
    cursor = g.conn.execute(
        "SELECT u.name as user, p.name as pos, o.name as org FROM referral r JOIN users u ON r.sender_id=u.user_id JOIN positions p ON p.position_id = r.pid JOIN organisations o ON o.organisation_id=r.oid WHERE receiver_id = %s",
        user_id)
    for result in cursor:
        referals.append({'user': result['user'], 'pos': result['pos'], 'org': result['org']})
    return referals


def get_org_names():
    organisations = []
    cursor = g.conn.execute("select name from organisations")
    for result in cursor:
        organisations.append(result[0])
    return organisations


def get_organisations():
    organisations = []
    cursor = g.conn.execute("select name, domain, number_of_employees from organisations")
    for result in cursor:
        organisations.append({'name': result[0], 'domain': result[1], 'number_of_employees': result[2]})
    return organisations


def get_organisation_id_and_position_id(organisation_name, position_name):
    cursor = g.conn.execute(
        "select position_id, oid from positions where name = %s and oid = (select organisation_id from organisations where name = %s)",
        position_name, organisation_name)
    for result in cursor:
        return result[0], result[1]


def get_org_id(organisation_name):
    cursor = g.conn.execute("select organisation_id from organisations where name = %s", organisation_name)
    for result in cursor:
        return result[0]


def get_pid_after_inserting_position(pos, description, location, salary, organisation_id):
    cursor = g.conn.execute("select position_id from positions where name = %s and oid = %s and location= %s", pos,
                            organisation_id, location)
    pids = []
    for result in cursor:
        pids.append(result[0])
    if len(pids) == 1:
        return pids[0]
    elif len(pids) < 1:
        pid = get_new_pos_id()
        g.conn.execute("insert into positions values (%s, %s, %s, %s, %s, %s)", pid, pos, description, location, salary,
                       organisation_id)
        return pid


def add_job_opening(oid, pid, name, uid, link):
    g.conn.execute(
        "insert into job_openings values (%s, %s, %s, %s, %s)", oid, pid, name, uid, link)


def post_referral(sender_id, receiver_id, org_id, pos_id, des):
    g.conn.execute(
        "insert into referral values (%s, %s, %s, %s, %s)", sender_id, receiver_id, org_id, pos_id, des)


def post_user(user_id, name, email, age, dob, work_ex, seeking, giving):
    g.conn.execute(
        "insert into users values (%s, %s, %s, %s, %s, %s, %s, %s)", user_id, name, email, age, dob, work_ex, seeking,
        giving)


def get_user_profile_details(user_id):
    details = dict()
    cursor = g.conn.execute(
        "SELECT u.name as user, u.email as email, u.age as age, u.work_experience as work_ex, s.name as skill_name FROM users u JOIN users_skill us ON us.user_id=u.user_id JOIN skills s ON s.skill_id = us.skill_id WHERE u.user_id = %s",
        user_id)
    for i, result in enumerate(cursor):
        if i == 0:
            details['user'] = result['user']
            details['email'] = result['email']
            details['age'] = result['age']
            details['work_ex'] = result['work_ex']
            details['skills'] = []
        details['skills'].append(result['skill_name'])
    details['skills'] = ', '.join(details['skills'])
    return details
